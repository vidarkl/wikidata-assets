package wikidata.assets;

import eu.optiquevqs.graph.*;
import eu.optiquevqs.graph.navigation.*;
// import eu.optiquevqs.graph.query.*;

import java.util.*;

public class WikidataAssets {

    public static NavigationGraph getNavigationGraph(String navigationGraphKey) {

        // Define class URIs
        String classActorUri            = "http://www.wikidata.org/entity/Q33999";
        String classAwardUri            = "http://www.wikidata.org/entity/Q618779";
        String classBookUri             = "http://www.wikidata.org/entity/Q571";
        String classCapitalUri          = "http://www.wikidata.org/entity/Q5119";
        String classCityUri             = "http://www.wikidata.org/entity/Q515";
        String classCityUSAUri          = "http://www.wikidata.org/entity/Q1093829";
        String classContinentUri        = "http://www.wikidata.org/entity/Q5107";
        String classCountryUri          = "http://www.wikidata.org/entity/Q6256";
        String classDictionaryEntryUri  = "http://www.wikidata.org/entity/Q4423781";
        String classEyeColorUri         = "http://www.wikidata.org/entity/Q23786";
        String classFemaleUri           = "http://www.wikidata.org/entity/Q6581072";
        String classFilmDirectorUri     = "http://www.wikidata.org/entity/Q2526255";
        String classFilmGenreUri        = "http://www.wikidata.org/entity/Q201658";
        String classFilmographyUri      = "http://www.wikidata.org/entity/Q1371849";
        String classFilmUri             = "http://www.wikidata.org/entity/Q11424";
        String classGenderUri           = "http://www.wikidata.org/entity/Q48277";
        String classHouseCatUri         = "http://www.wikidata.org/entity/Q146";
        String classHumanHairColorUri   = "http://www.wikidata.org/entity/Q1048314";
        String classHumanUri            = "http://www.wikidata.org/entity/Q5";
        String classMaleUri             = "http://www.wikidata.org/entity/Q6581097";
        String classMunicipalityUri     = "http://www.wikidata.org/entity/Q15284";
        String classPaintingUri         = "http://www.wikidata.org/entity/Q3305213";
        String classProfessionUri       = "http://www.wikidata.org/entity/Q28640";
        String classTelevisionSeriesUri = "http://www.wikidata.org/entity/Q5398426";

        // Define datatype URIs
        String datatypeStringUri   = "String";
        String datatypeIntegerUri  = "Integer";
        String datatypeDoubleUri   = "Double";
        String datatypeDatetimeUri = "Datetime";
        String datatypeLocationUri = "Location";

        // Define property URIs
        String aboutUri                 = "http://schema.org/about";
        String awardReceivedUri         = "http://www.wikidata.org/prop/direct/P166";
        String birthNameUri             = "http://www.wikidata.org/prop/direct/P1477";
        String capitalUri               = "http://www.wikidata.org/prop/direct/P36";
        String castMemberUri            = "http://www.wikidata.org/prop/direct/P161";
        String childUri                 = "http://www.wikidata.org/prop/direct/P40";
        String coordinateLocationUri    = "http://www.wikidata.org/prop/direct/P625";
        String countryOfCitzenshipUri   = "http://www.wikidata.org/prop/direct/P27";
        String countryOfOriginUri       = "http://www.wikidata.org/prop/direct/P495";
        String creatorUri               = "http://www.wikidata.org/prop/direct/P170";
        String dateOfBirthUri           = "http://www.wikidata.org/prop/direct/P569";
        String descriptionUri           = "http://schema.org/description";
        String diplomaticRelationUri    = "http://www.wikidata.org/prop/direct/P530";
        String directorUri              = "http://www.wikidata.org/prop/direct/P57";
        String endTimeUri               = "http://www.wikidata.org/prop/direct/P582";
        String eyeColorUri              = "http://www.wikidata.org/prop/direct/P1340";
        String familyNameUri            = "http://www.wikidata.org/prop/direct/P734";
        String filmingLocationUri       = "http://www.wikidata.org/prop/direct/P915";
        String filmographyUri           = "http://www.wikidata.org/prop/direct/P1283";
        String genreUri                 = "http://www.wikidata.org/prop/direct/P136";
        String givenNameUri             = "http://www.wikidata.org/prop/direct/P735";
        String hairColorUri             = "http://www.wikidata.org/prop/direct/P1884";
        String headOfGovernmentUri      = "http://www.wikidata.org/prop/direct/P6";
        String headOfStateUri           = "http://www.wikidata.org/prop/direct/P35";
        String heightUri                = "http://www.wikidata.org/prop/direct/P2048";
        String inceptionUri             = "http://www.wikidata.org/prop/direct/P571";
        String labelUri                 = "http://www.w3.org/2000/01/rdf-schema#label";
        String lifeExpectancyUri        = "http://www.wikidata.org/prop/direct/P2250";
        String locatedInUri             = "http://www.wikidata.org/prop/direct/P131";
        String mediaTitleUri            = "http://www.wikidata.org/prop/direct/P1476";
        String nameNativeLanguageUri    = "http://www.wikidata.org/prop/direct/P1559";
        String nameUri                  = "http://www.wikidata.org/prop/direct/P2561";
        String numberOfChildrenUri      = "http://www.wikidata.org/prop/direct/P1971";
        String occupationUri            = "http://www.wikidata.org/prop/direct/P106";
        String officialNameUri          = "http://www.wikidata.org/prop/direct/P1448";
        String partOfUri                = "http://www.wikidata.org/prop/direct/P361";
        String placeOfBirthUri          = "http://www.wikidata.org/prop/direct/P19";
        String populationUri            = "http://www.wikidata.org/prop/direct/P1082";
        String residenceUri             = "http://www.wikidata.org/prop/direct/P551";
        String screenWriterUri          = "http://www.wikidata.org/prop/direct/P58";
        String sexOrGenderUri           = "http://www.wikidata.org/prop/direct/P21";
        String sharesBorderWithUri      = "http://www.wikidata.org/prop/direct/P47";
        String siblingUri               = "http://www.wikidata.org/prop/direct/P3373";
        String spouseUri                = "http://www.wikidata.org/prop/direct/P26";
        String startTimeUri             = "http://www.wikidata.org/prop/direct/P580";
        String studentOfUri             = "http://www.wikidata.org/prop/direct/P1066";
        String workLocationUri          = "http://www.wikidata.org/prop/direct/P937";


        if (navigationGraphKey.equals("wikidata-1")) {

            NavigationGraph navigationGraph = new NavigationGraph();

            // Define classes
            ClassNode classHuman            = new ClassNode(classHumanUri);
            ClassNode classFilm             = new ClassNode(classFilmUri);
            ClassNode classCountry          = new ClassNode(classCountryUri);
            ClassNode classActor            = new ClassNode(classActorUri);
            ClassNode classFilmDirector     = new ClassNode(classFilmDirectorUri);
            ClassNode classMale             = new ClassNode(classMaleUri);
            ClassNode classFemale           = new ClassNode(classFemaleUri);
            ClassNode classTelevisionSeries = new ClassNode(classTelevisionSeriesUri);
            ClassNode classCity             = new ClassNode(classCityUri);
            ClassNode classFilmography      = new ClassNode(classFilmographyUri);
            ClassNode classCapital          = new ClassNode(classCapitalUri);
            ClassNode classDictionaryEntry  = new ClassNode(classDictionaryEntryUri);
            ClassNode classHouseCat         = new ClassNode(classHouseCatUri);
            ClassNode classPainting         = new ClassNode(classPaintingUri);
            ClassNode classMunicipality     = new ClassNode(classMunicipalityUri);
            ClassNode classBook             = new ClassNode(classBookUri);
            ClassNode classGender           = new ClassNode(classGenderUri);
            ClassNode classProfession       = new ClassNode(classProfessionUri);
            ClassNode classCityUSA          = new ClassNode(classCityUSAUri);
            ClassNode classEyeColor         = new ClassNode(classEyeColorUri);

            // Define datatypes
            DatatypeNode datatypeString   = new DatatypeNode(datatypeStringUri);
            DatatypeNode datatypeInteger  = new DatatypeNode(datatypeIntegerUri);
            DatatypeNode datatypeDouble   = new DatatypeNode(datatypeDoubleUri);
            DatatypeNode datatypeDatetime = new DatatypeNode(datatypeDatetimeUri);

            // Add classes
            navigationGraph.addVertex(classHuman);
            navigationGraph.addVertex(classFilm);
            navigationGraph.addVertex(classCountry);
            navigationGraph.addVertex(classActor);
            navigationGraph.addVertex(classFilmDirector);
            navigationGraph.addVertex(classMale);
            navigationGraph.addVertex(classFemale);
            navigationGraph.addVertex(classTelevisionSeries);
            navigationGraph.addVertex(classCity);
            navigationGraph.addVertex(classFilmography);
            navigationGraph.addVertex(classCapital);
            navigationGraph.addVertex(classDictionaryEntry);
            navigationGraph.addVertex(classHouseCat);
            navigationGraph.addVertex(classPainting);
            navigationGraph.addVertex(classMunicipality);
            navigationGraph.addVertex(classBook);
            navigationGraph.addVertex(classGender);
            navigationGraph.addVertex(classProfession);
            navigationGraph.addVertex(classCityUSA);
            navigationGraph.addVertex(classEyeColor);

            // Add datatypes
            navigationGraph.addVertex(datatypeString);
            navigationGraph.addVertex(datatypeInteger);
            navigationGraph.addVertex(datatypeDouble);
            navigationGraph.addVertex(datatypeDatetime);


            // Add edges like this: (source, target, PropertyEdge)
            navigationGraph.addEdge(classHuman, datatypeString,   new PropertyEdge(birthNameUri));
            navigationGraph.addEdge(classHuman, datatypeString,   new PropertyEdge(givenNameUri));
            navigationGraph.addEdge(classHuman, datatypeString,   new PropertyEdge(familyNameUri));
            navigationGraph.addEdge(classHuman, datatypeString,   new PropertyEdge(nameNativeLanguageUri));
            navigationGraph.addEdge(classHuman, datatypeString,   new PropertyEdge(heightUri));
            navigationGraph.addEdge(classHuman, datatypeString,   new PropertyEdge(eyeColorUri));
            navigationGraph.addEdge(classHuman, datatypeString,   new PropertyEdge(hairColorUri));
            navigationGraph.addEdge(classHuman, datatypeString,   new PropertyEdge(labelUri));
            navigationGraph.addEdge(classHuman, datatypeString,   new PropertyEdge(aboutUri));
            navigationGraph.addEdge(classHuman, datatypeString,   new PropertyEdge(descriptionUri));
            navigationGraph.addEdge(classHuman, datatypeInteger,  new PropertyEdge(numberOfChildrenUri));
            navigationGraph.addEdge(classHuman, classGender,      new PropertyEdge(sexOrGenderUri));
            navigationGraph.addEdge(classHuman, classHuman,       new PropertyEdge(childUri));
            navigationGraph.addEdge(classHuman, classHuman,       new PropertyEdge(spouseUri));
            navigationGraph.addEdge(classHuman, classHuman,       new PropertyEdge(siblingUri));
            navigationGraph.addEdge(classHuman, classCountry,     new PropertyEdge(countryOfCitzenshipUri));
            navigationGraph.addEdge(classHuman, classProfession,  new PropertyEdge(occupationUri));
            navigationGraph.addEdge(classHuman, classFilmography, new PropertyEdge(filmographyUri));

            return navigationGraph;
        }


        else if (navigationGraphKey.equals("wikidata-2")) {

            NavigationGraph navigationGraph = new NavigationGraph();

            // Define classes
            ClassNode classHuman            = new ClassNode(classHumanUri);
            ClassNode classFilm             = new ClassNode(classFilmUri);
            ClassNode classCountry          = new ClassNode(classCountryUri);
            ClassNode classTelevisionSeries = new ClassNode(classTelevisionSeriesUri);
            ClassNode classCity             = new ClassNode(classCityUri);
            ClassNode classCapital          = new ClassNode(classCapitalUri);
            ClassNode classGender           = new ClassNode(classGenderUri);
            ClassNode classProfession       = new ClassNode(classProfessionUri);
            ClassNode classCityUSA          = new ClassNode(classCityUSAUri);
            ClassNode classEyeColor         = new ClassNode(classEyeColorUri);
            ClassNode classFilmography      = new ClassNode(classFilmographyUri);
            ClassNode classHumanHairColor   = new ClassNode(classHumanHairColorUri);
            ClassNode classAward            = new ClassNode(classAwardUri);
            ClassNode classFilmGenre        = new ClassNode(classFilmGenreUri);
            ClassNode classContinent        = new ClassNode(classContinentUri);

            // Define datatypes
            DatatypeNode datatypeString   = new DatatypeNode(datatypeStringUri);
            DatatypeNode datatypeInteger  = new DatatypeNode(datatypeIntegerUri);
            DatatypeNode datatypeDouble   = new DatatypeNode(datatypeDoubleUri);
            DatatypeNode datatypeDatetime = new DatatypeNode(datatypeDatetimeUri);
            DatatypeNode datatypeLocation = new DatatypeNode(datatypeLocationUri);


            // Add classes
            navigationGraph.addVertex(classHuman);
            navigationGraph.addVertex(classFilm);
            navigationGraph.addVertex(classCountry);
            navigationGraph.addVertex(classTelevisionSeries);
            navigationGraph.addVertex(classCity);
            navigationGraph.addVertex(classCapital);
            navigationGraph.addVertex(classGender);
            navigationGraph.addVertex(classProfession);
            navigationGraph.addVertex(classCityUSA);
            navigationGraph.addVertex(classEyeColor);
            navigationGraph.addVertex(classFilmography);
            navigationGraph.addVertex(classHumanHairColor);
            navigationGraph.addVertex(classAward);
            navigationGraph.addVertex(classFilmGenre);
            navigationGraph.addVertex(classContinent);

            // Add datatypes
            navigationGraph.addVertex(datatypeString);
            navigationGraph.addVertex(datatypeInteger);
            navigationGraph.addVertex(datatypeDouble);
            navigationGraph.addVertex(datatypeDatetime);
            navigationGraph.addVertex(datatypeLocation);

            navigationGraph.addEdge(classHuman,            datatypeString,      new PropertyEdge(birthNameUri));
            navigationGraph.addEdge(classHuman,            datatypeString,      new PropertyEdge(givenNameUri));
            navigationGraph.addEdge(classHuman,            datatypeString,      new PropertyEdge(familyNameUri));
            navigationGraph.addEdge(classHuman,            datatypeString,      new PropertyEdge(nameNativeLanguageUri));
            navigationGraph.addEdge(classHuman,            datatypeInteger,     new PropertyEdge(numberOfChildrenUri));
            navigationGraph.addEdge(classHuman,            classGender,         new PropertyEdge(sexOrGenderUri));
            navigationGraph.addEdge(classHuman,            classHuman,          new PropertyEdge(childUri));
            navigationGraph.addEdge(classHuman,            classHuman,          new PropertyEdge(spouseUri));
            navigationGraph.addEdge(classHuman,            classHuman,          new PropertyEdge(siblingUri));
            navigationGraph.addEdge(classHuman,            classHuman,          new PropertyEdge(studentOfUri));
            navigationGraph.addEdge(classHuman,            classEyeColor,       new PropertyEdge(eyeColorUri));
            navigationGraph.addEdge(classHuman,            datatypeDatetime,    new PropertyEdge(dateOfBirthUri));
            navigationGraph.addEdge(classHuman,            datatypeString,      new PropertyEdge(heightUri));
            navigationGraph.addEdge(classHuman,            classProfession,     new PropertyEdge(occupationUri));
            navigationGraph.addEdge(classHuman,            classCity,           new PropertyEdge(residenceUri));
            navigationGraph.addEdge(classHuman,            classCityUSA,        new PropertyEdge(residenceUri));
            navigationGraph.addEdge(classHuman,            classHumanHairColor, new PropertyEdge(hairColorUri));
            navigationGraph.addEdge(classHuman,            classAward,          new PropertyEdge(awardReceivedUri));
            navigationGraph.addEdge(classHuman,            classFilmography,    new PropertyEdge(filmographyUri));
            navigationGraph.addEdge(classHuman,            classCountry,        new PropertyEdge(countryOfCitzenshipUri));
            navigationGraph.addEdge(classHuman,            classCity,           new PropertyEdge(placeOfBirthUri));
            navigationGraph.addEdge(classHuman,            classCityUSA,        new PropertyEdge(placeOfBirthUri));
            navigationGraph.addEdge(classHuman,            classCity,           new PropertyEdge(workLocationUri));
            navigationGraph.addEdge(classHuman,            classCityUSA,        new PropertyEdge(workLocationUri));
            navigationGraph.addEdge(classFilm,             datatypeString,      new PropertyEdge(mediaTitleUri));
            navigationGraph.addEdge(classFilm,             classFilmGenre,      new PropertyEdge(genreUri));
            navigationGraph.addEdge(classFilm,             classCountry,        new PropertyEdge(countryOfOriginUri));
            navigationGraph.addEdge(classFilm,             classHuman,          new PropertyEdge(castMemberUri));
            navigationGraph.addEdge(classFilm,             classCity,           new PropertyEdge(filmingLocationUri));
            navigationGraph.addEdge(classFilm,             classCityUSA,        new PropertyEdge(filmingLocationUri));
            navigationGraph.addEdge(classFilm,             classAward,          new PropertyEdge(awardReceivedUri));
            navigationGraph.addEdge(classFilm,             classHuman,          new PropertyEdge(directorUri));
            navigationGraph.addEdge(classTelevisionSeries, datatypeString,      new PropertyEdge(mediaTitleUri));
            navigationGraph.addEdge(classTelevisionSeries, datatypeString,      new PropertyEdge(genreUri));
            navigationGraph.addEdge(classTelevisionSeries, classHuman,          new PropertyEdge(creatorUri));
            navigationGraph.addEdge(classTelevisionSeries, classCountry,        new PropertyEdge(countryOfOriginUri));
            navigationGraph.addEdge(classTelevisionSeries, classHuman,          new PropertyEdge(screenWriterUri));
            navigationGraph.addEdge(classTelevisionSeries, classHuman,          new PropertyEdge(castMemberUri));
            navigationGraph.addEdge(classTelevisionSeries, classAward,          new PropertyEdge(awardReceivedUri));
            navigationGraph.addEdge(classTelevisionSeries, datatypeDatetime,    new PropertyEdge(startTimeUri));
            navigationGraph.addEdge(classTelevisionSeries, datatypeDatetime,    new PropertyEdge(endTimeUri));
            navigationGraph.addEdge(classCountry,          classContinent,      new PropertyEdge(partOfUri));
            navigationGraph.addEdge(classCountry,          datatypeDatetime,    new PropertyEdge(inceptionUri));
            navigationGraph.addEdge(classCountry,          datatypeString,      new PropertyEdge(lifeExpectancyUri));
            navigationGraph.addEdge(classCountry,          datatypeString,      new PropertyEdge(officialNameUri));
            navigationGraph.addEdge(classCountry,          datatypeLocation,    new PropertyEdge(coordinateLocationUri));
            navigationGraph.addEdge(classCountry,          classHuman,          new PropertyEdge(headOfStateUri));
            navigationGraph.addEdge(classCountry,          classHuman,          new PropertyEdge(headOfGovernmentUri));
            navigationGraph.addEdge(classCountry,          classCountry,        new PropertyEdge(diplomaticRelationUri));
            navigationGraph.addEdge(classCountry,          datatypeInteger,     new PropertyEdge(populationUri));
            navigationGraph.addEdge(classCountry,          classCountry,        new PropertyEdge(sharesBorderWithUri));
            navigationGraph.addEdge(classCity,             datatypeDatetime,    new PropertyEdge(inceptionUri));
            navigationGraph.addEdge(classCity,             datatypeString,      new PropertyEdge(officialNameUri));
            navigationGraph.addEdge(classCity,             datatypeLocation,    new PropertyEdge(coordinateLocationUri));
            navigationGraph.addEdge(classCity,             classHuman,          new PropertyEdge(headOfGovernmentUri));
            navigationGraph.addEdge(classCity,             classAward,          new PropertyEdge(awardReceivedUri));
            navigationGraph.addEdge(classCity,             datatypeInteger,     new PropertyEdge(populationUri));
            navigationGraph.addEdge(classCityUSA,          datatypeDatetime,    new PropertyEdge(inceptionUri));
            navigationGraph.addEdge(classCityUSA,          datatypeString,      new PropertyEdge(officialNameUri));
            navigationGraph.addEdge(classCityUSA,          datatypeLocation,    new PropertyEdge(coordinateLocationUri));
            navigationGraph.addEdge(classCityUSA,          classHuman,          new PropertyEdge(headOfGovernmentUri));
            navigationGraph.addEdge(classCityUSA,          classAward,          new PropertyEdge(awardReceivedUri));
            navigationGraph.addEdge(classCityUSA,          datatypeInteger,     new PropertyEdge(populationUri));
            navigationGraph.addEdge(classContinent,        datatypeLocation,    new PropertyEdge(coordinateLocationUri));
            navigationGraph.addEdge(classContinent,        datatypeInteger,     new PropertyEdge(populationUri));
            navigationGraph.addEdge(classContinent,        classContinent,      new PropertyEdge(sharesBorderWithUri));
            navigationGraph.addEdge(classCountry,          classCity,           new PropertyEdge(capitalUri));

            return navigationGraph;
        }


        else {
            return null;
        }
    }

}
