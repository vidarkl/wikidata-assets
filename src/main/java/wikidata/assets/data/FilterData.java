package wikidata.assets.data;

// In this file we take a ttl-file in, and go through each line one by one and keep it only if it satisfies the requirements
// It takes about 40 minutes to run this over 1B triples like e.g the wikidata dataset from 2015

import eu.optiquevqs.graph.*;
import eu.optiquevqs.graph.navigation.*;

import wikidata.assets.*;
import wikidata.assets.queries.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.jena.rdf.model.ModelFactory;
import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.*;



public class FilterData {

    public static void main(String[] args) throws Exception {

        // Settings
        boolean restrictDataToNavigationGraph = true; // Restrict to what exists in the allowed predicates and classes.
        boolean removeNonEnglish = true;  // Set true to remove everything that is not english. Seems to reduce to about one tenth of the original wikidata dataset
        boolean limitNumberOfLines = false;  // Set to limit the number of lines we look at. Mostly for debugging
        int maxLines = 0;  // How many lines to look over.

        Set<String> prefixesToRemove = new HashSet<String>();
        prefixesToRemove.add("v:"); // Entity value
        prefixesToRemove.add("s:"); // Statements
        prefixesToRemove.add("ref:"); // References
        prefixesToRemove.add("data:"); // References

        Set<String> containsToRemove = new HashSet<String>();
        containsToRemove.add("wikipedia.org"); // Wikipedia
        containsToRemove.add("wikiquote.org"); // Wikiquote
        containsToRemove.add("wikinews.org"); // Wikinews
        containsToRemove.add("wikibooks.org"); // Wikibooks
        containsToRemove.add("wikisource.org"); // Wikibooks
        containsToRemove.add("wikivoyage.org"); // Wikibooks
        containsToRemove.add("wikimedia.org"); // Wikibooks

        // Set the input file path
        String filePath = "./files/data/original/wikidata-20150420-all-BETA.ttl";
        String outputFilename = "./files/data/output/wikidata_filtered.ttl";

        // Get the navigation graph
        NavigationGraph navigationGraph = WikidataAssets.getNavigationGraph("wikidata-2");

        // Define prefixes used in the datafile
        // Seems like wikidata dataset I have uses old prefixes. while queries uses new...
        // So here we can redefine the prefixes if we want to.
        Map<String, String> prefixes = new HashMap<String, String>();

        prefixes.put("xsd", "http://www.w3.org/2001/XMLSchema#");
        prefixes.put("cc", "http://creativecommons.org/ns#");
        prefixes.put("data", "http://www.wikidata.org/wiki/Special:EntityData/");
        prefixes.put("entity", "http://www.wikidata.org/entity/");
        prefixes.put("geo", "http://www.opengis.net/ont/geosparql#");
        prefixes.put("prov", "http://www.w3.org/ns/prov#");
        prefixes.put("q", "http://www.wikidata.org/entity/qualifier/");
        prefixes.put("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
        prefixes.put("ref", "http://www.wikidata.org/entity/reference/");
        prefixes.put("s", "http://www.wikidata.org/entity/statement/");
        prefixes.put("schema", "http://schema.org/");
        prefixes.put("skos", "http://www.w3.org/2004/02/skos/core#");
        prefixes.put("v", "http://www.wikidata.org/entity/value/");
        prefixes.put("wdt", "http://www.wikidata.org/prop/direct/");
        prefixes.put("wikibase", "http://www.wikidata.org/ontology-0.0.1#");


        // Collect allowed properties from navigation graph
        ArrayList<String> allowedProperties = new ArrayList<String>();
        for (PropertyEdge pe : navigationGraph.edgeSet()) {
            allowedProperties.add(pe.getLabel());
        }

        ArrayList<String> allowedClasses = new ArrayList<String>();
        for (NavigationGraphNode n: navigationGraph.vertexSet()) {
            allowedClasses.add(n.getLabel());
        }

        // Put them in map for fast lookup, also add prefix version if possible
        Map<String, Boolean> allowedPredicatesMap = new HashMap<String, Boolean>();
        for (String predicate : allowedProperties) {
            allowedPredicatesMap.put("<" + predicate + ">", true);
            for (Map.Entry<String, String> pair : prefixes.entrySet()) {
                String prefix = pair.getKey();
                String expansion = pair.getValue();
                if (predicate.contains(expansion)) {
                    String predicateWithPrefix = predicate.replace(expansion, prefix + ":");
                    allowedPredicatesMap.put(predicateWithPrefix, true);
                }
            }
        }

        // Put them in map for fast lookup, also add prefix version if possible
        Map<String, Boolean> allowedClassesMap = new HashMap<String, Boolean>();
        for (String cls : allowedClasses) {
            allowedClassesMap.put("<" + cls + ">", true);
            for (Map.Entry<String, String> pair : prefixes.entrySet()) {
                String prefix = pair.getKey();
                String expansion = pair.getValue();
                if (cls.contains(expansion)) {
                    String classWithPrefix = cls.replace(expansion, prefix + ":");
                    allowedClassesMap.put(classWithPrefix, true);
                }
            }
        }


        // Build the prefix block
        String prefixBlock = "";
        for (Map.Entry<String, String> pair : prefixes.entrySet()) {
            prefixBlock += "@prefix "  + pair.getKey() + ": <" + pair.getValue() + "> .\n";
        }



        // Just write the prefix block to the file
        FileWriter fw1 = new FileWriter(outputFilename);
        fw1.write(prefixBlock + "\n"); //appends the prefix block to the file
        fw1.close();
        FileWriter fw = new FileWriter(outputFilename, true); //the true will append the new data

        // control variables needed for controlling the process.
        String lastLineEnd = ".";
        String readLine = "";
        boolean errorPause = false;
        String tripleLanguage = null;

        // triple parts
        String subject = null;
        String predicate = null;
        String object = null;


        // counts for statistics.
        int failToSplitIntoTwoCounter = 0;
        int lineCounter = 0;
        int emptyLineCounter = 0;
        int prefixCounter = 0;
        int notTripleCounter = 0;
        int skippedByErrorPauseCounter = 0;
        int wrongLanguageCounter = 0;
        int wrongPrefixCounter = 0;
        int wrongContainsCounter = 0;
        int slashInUrlCounter = 0;
        int skipSpaceInSubjectOrPredicateCount = 0;
        int writeCount = 0;

        // Collect info about which triples are gathered for statistics
        Map <String, Integer> collectedPredicates = new HashMap<String, Integer>();
        Map <String, Integer> collectedClasses = new HashMap<String, Integer>();

        File f = new File(filePath);

        boolean print = false;
        final long startTime = System.currentTimeMillis();
        try ( BufferedReader b = new BufferedReader(new FileReader(f))   ) {
            while ((readLine = b.readLine()) != null) {  // Loop over lines
                lineCounter+= 1;

                // Stop if we have reached the max number of lines
                if (limitNumberOfLines) {
                    if (lineCounter >= maxLines) break;
                }

                // Skip the empty lines
                if ("".equals(readLine)){ 
                    emptyLineCounter += 1;
                    continue; 
                }

                // Skip all prefix lines
                if (readLine.startsWith("@prefix")) { 
                    prefixCounter += 1;
                    continue; 
                }

                String lastChar = readLine.substring(readLine.length() - 1); // Get last character in line
                readLine = readLine.substring(0, readLine.length()-1).trim();  // slightly modify the line. Taking away last char. Also trim it

                // If we do not get a reasonable last character on last line, we have to skip until we reach a line after one ending with period
                if (!(lastChar.equals(".") || lastChar.equals(";") || lastChar.equals(","))) {
                    System.out.println("ERROR ON LASTCHAR: " + lastChar);
                    System.out.println("line: " + lineCounter);
                    errorPause = true;
                }

                // If we have an error pause, we have to skip this line
                // But if the last character is a period, we will stop pausing
                if (errorPause) { 
                    skippedByErrorPauseCounter += 1;
                    System.out.println("Skipping");
                    if (lastChar.equals(".")) { errorPause = false; }
                    continue; 
                }



                boolean skipBecauseNotTriple = false;
                // Cases: Either . , or ; as last line end
                if (".".equals(lastLineEnd)) {
                    // In case of three parts
                    String [] l = readLine.split(" (?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");   // If last ended with period, then we have three parts. 

                    if (l.length != 3) {
                        skipBecauseNotTriple = true;
                    }
                    else {
                        subject = l[0];
                        predicate = l[1];
                        object = l[2];
                    }

                }
                else if (";".equals(lastLineEnd)) {
                    // In the case where we expect two parts
                    String [] l = readLine.split(" (?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");  // if last ended with ;, then only two parts
                    if (l.length != 2) {
                        skipBecauseNotTriple = true;
                    }
                    else {
                        predicate = l[0];
                        object = l[1];
                    }
                }
                // In case of one part and list
                else if (",".equals(lastLineEnd)) {
                    String [] l = readLine.split(" (?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                    if (l.length != 1) {
                        skipBecauseNotTriple = true;
                    }
                    else {
                        object = l[0];
                    }
                }

                // Save the last character
                lastLineEnd = lastChar;

                if (skipBecauseNotTriple) {
                    notTripleCounter += 1;
                    continue;
                }

                // We want to use rdf:type instead of a and the wikidata P31
                if ("wdt:P31".equals(predicate) || "v:P31".equals(predicate) || "a".equals(predicate) || "<http://www.wikidata.org/prop/direct/P31>".equals(predicate)) {
                    predicate = "<" + Utils.getRdfTypePredicate() + ">";
                }

                if (predicate.contains(" ") || subject.contains(" ")) {
                    skipSpaceInSubjectOrPredicateCount += 1;
                    continue;
                }

                // Check if any of the components has invalid uris using \
                Pattern p = Pattern.compile("^\\<(.*)\\\\(.*)\\>$");
                Matcher ma = p.matcher(object);
                if (ma.find()) {
                    System.out.println("Found slash in url");
                    slashInUrlCounter += 1;
                    continue;
                }

                // Part finding the triple language. I.e. language of literal object
                Pattern languagePattern = Pattern.compile("@([a-z-]*)$");
                Matcher m = languagePattern.matcher(object);
                tripleLanguage = null;
                if (m.find()) {
                    tripleLanguage = m.group(1);
                }

                // If we only restrict on english language, then we have to check that the triple is either english or non-existent
                if (removeNonEnglish) {
                    if (tripleLanguage != null) {
                        if (!(tripleLanguage.equals("en"))) {
                            wrongLanguageCounter += 1;
                            continue;
                        }
                    }
                }


                // Remove triples where any of the components are not allowed 
                boolean skipTriple = false;
                for (String prefix : prefixesToRemove) {
                    if (subject.startsWith(prefix)) skipTriple = true;
                    if (predicate.startsWith(prefix)) skipTriple = true;
                    if (object.startsWith(prefix)) skipTriple = true;
                }
                if (skipTriple) {
                    wrongPrefixCounter += 1;
                    continue;
                }
                for (String substr : containsToRemove) {
                    if (subject.contains(substr)) skipTriple = true;
                    if (predicate.contains(substr)) skipTriple = true;
                    if (object.contains(substr)) skipTriple = true;
                }
                if (skipTriple) {
                    wrongContainsCounter += 1;
                    continue;
                }

                // At this point we have the whole triple, including an optional language tag.



                // Increase count of the current predicate for statistics
                Integer v = collectedPredicates.getOrDefault(predicate,0);
                collectedPredicates.put(predicate, v+1);

                // Increase count of the current predicate for statistics
                if (("<" +Utils.getRdfTypePredicate() + ">").equals(predicate)) {
                    Integer w = collectedClasses.getOrDefault(object,0);
                    collectedClasses.put(object, w+1);
                }


                // If triple is a type statement, and the class is allowed, we keep it
                if (("<" + Utils.getRdfTypePredicate() + ">").equals(predicate)) {
                    if ( allowedClassesMap.getOrDefault(object, false) || !restrictDataToNavigationGraph ) {
                        fw.write(subject + " " + predicate + " " + object + " .\n"); //appends the string to the file
                        writeCount += 1;
                    }
                }
                else {
                    // If triple contains an allowed predicate, we keep it as well
                    if ( allowedPredicatesMap.getOrDefault(predicate, false) || !restrictDataToNavigationGraph ) {
                        fw.write(subject + " " + predicate + " " + object + " .\n");//appends the string to the file
                        writeCount += 1;
                    }
                }

            }
        }

        // After looping over all lines, print status
        final long endTime = System.currentTimeMillis();

        System.out.println("\nMost used predicates (over all triples in the file, not only those fitting ng)");
        // Print the counts ordered by value
        collectedPredicates.entrySet().stream().sorted((k1, k2) -> -k1.getValue().compareTo(k2.getValue()))
                .limit(100).forEach(k -> System.out.println(k.getKey() + "\t" +  k.getValue() ));

        System.out.println("\nMost used classes (over all triples in the file, not only those fitting ng)");
        // Print the counts ordered by value
        collectedClasses.entrySet().stream().sorted((k1, k2) -> -k1.getValue().compareTo(k2.getValue()))
                .limit(100).forEach(k -> System.out.println(k.getKey() + "\t" +  k.getValue() ));

        System.out.println();
        System.out.println("-------------------------------------------------------");
        System.out.println("Total fails to split into two: " + failToSplitIntoTwoCounter);
        System.out.println("Total lines read(maxlines): " + maxLines);
        System.out.println("Total empty lines: " + emptyLineCounter);
        System.out.println("Total skipped because not triple: " + notTripleCounter);
        System.out.println("Total number of prefixes: " + prefixCounter);
        System.out.println("Total skipped by error pause: " + skippedByErrorPauseCounter);
        System.out.println("Wrong prefix: " + wrongPrefixCounter);
        System.out.println("Wrong contain: " + wrongContainsCounter);
        System.out.println("Slash in url: " + slashInUrlCounter);
        System.out.println("Skip space in predicate: " + skipSpaceInSubjectOrPredicateCount);
        System.out.println("Wrong language: " + wrongLanguageCounter);
        System.out.println("Total number of lines in new file: " + writeCount);
        System.out.println("-------------------------------------------------------");
        System.out.println("Total execution time: " + (endTime - startTime));
        System.out.println("-------------------------------------------------------");

        fw.close();
    }
}


