package wikidata.assets.queries.visitors;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.*;
import java.util.*;
import java.lang.Boolean;

// Jena ARQ
import org.apache.jena.query.*;
import org.apache.jena.sparql.algebra.*;
import org.apache.jena.sparql.algebra.op.*;

import org.apache.jena.sparql.core.*;
import org.apache.jena.sparql.path.*;
import org.apache.jena.graph.Triple;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;

import org.apache.jena.sparql.core.*;
import org.apache.jena.sparql.path.*;
import org.apache.jena.sparql.expr.*;
import org.apache.jena.sparql.expr.nodevalue.*;


// This visitor has one simple task:
// Check that the query only contains only valid parts: triples and simple filters.
public class CheckVQSQueryVisitor extends OpVisitorBase {

    // A map from each op to a number indicating how many there are.
    public boolean isVQS = true;

    public boolean getResult() {
        return isVQS;
    }

    @Override
    public void visit(OpFilter op) {
        ExprList exps = op.getExprs();
        for (Expr expr : exps) {
            if (expr instanceof E_GreaterThan || expr instanceof E_LessThan || expr instanceof E_GreaterThanOrEqual || expr instanceof E_LessThanOrEqual || expr instanceof E_Equals) { 

                Expr leftSide = ((ExprFunction2)expr).getArg1();
                Expr rightSide = ((ExprFunction2)expr).getArg2();
                if ((leftSide instanceof ExprVar && rightSide instanceof NodeValue) || (leftSide instanceof NodeValue && rightSide instanceof ExprVar)) {}
                else { isVQS = false; }
            }
            else { isVQS = false; }
        }
    }

    @Override
    public void visit(OpBGP op) {
    }

    @Override
    public void visit(OpAssign op) {
        isVQS = false;
    }

    @Override
    public void visit(OpConditional op) {
        isVQS = false;
    }

    @Override
    public void visit(OpDatasetNames op) {
        isVQS = false;
    }

    @Override
    public void visit(OpDiff op) {
        isVQS = false;
    }

    @Override
    public void visit(OpDisjunction op) {
        isVQS = false;
    }

    @Override
    public void visit(OpDistinct op) {
        isVQS = false;
    }

    @Override
    public void visit(OpExtend op) {
        isVQS = false;
    }

    @Override
    public void visit(OpGraph op) {
        isVQS = false;
    }

    @Override
    public void visit(OpGroup op) {
        isVQS = false;
    }

    @Override
    public void visit(OpJoin op) {
        isVQS = false;
    }

    @Override
    public void visit(OpLabel op) {
        isVQS = false;
    }

    @Override
    public void visit(OpLeftJoin op) {
        isVQS = false;
    }

    @Override
    public void visit(OpList op) {
        isVQS = false;
    }

    @Override
    public void visit(OpMinus op) {
        isVQS = false;
    }

    @Override
    public void visit(OpNull op) {
        isVQS = false;
    }

    @Override
    public void visit(OpOrder op) {
        isVQS = false;
    }

    @Override
    public void visit(OpPath op) {
        isVQS = false;
    }

    @Override
    public void visit(OpProcedure op) {
        isVQS = false;
    }

    @Override
    public void visit(OpProject op) {
        isVQS = false;
    }

    @Override
    public void visit(OpPropFunc op) {
        isVQS = false;
    }

    @Override
    public void visit(OpQuad op) {
        isVQS = false;
    }

    @Override
    public void visit(OpQuadBlock op) {
        isVQS = false;
    }

    @Override
    public void visit(OpQuadPattern op) {
        isVQS = false;
    }

    @Override
    public void visit(OpReduced op) {
        isVQS = false;
    }

    @Override
    public void visit(OpSequence op) {
        isVQS = false;
    }

    @Override
    public void visit(OpService op) {
        isVQS = false;
    }

    @Override
    public void visit(OpSlice op) {
        isVQS = false;
    }

    @Override
    public void visit(OpTable op) {
        isVQS = false;
    }

    @Override
    public void visit(OpTopN op) {
        isVQS = false;
    }

    @Override
    public void visit(OpTriple op) {
        isVQS = false;
    }

    @Override
    public void visit(OpUnion op) {
        isVQS = false;
    }



}
