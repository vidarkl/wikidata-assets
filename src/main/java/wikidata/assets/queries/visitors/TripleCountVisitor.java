package wikidata.assets.queries.visitors;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.*;
import java.util.*;
import java.lang.Boolean;

// Jena ARQ
import org.apache.jena.query.*;
import org.apache.jena.sparql.algebra.*;
import org.apache.jena.sparql.algebra.op.*;

import org.apache.jena.sparql.core.BasicPattern;
import org.apache.jena.graph.Triple;


// This visitor assumes that the op only has one basic graph pattern, and only simple filters, i.e. no bgps in the filters.
// It counts how many triples a query contains
public class TripleCountVisitor extends OpVisitorBase {
    int counter = 0;

    @Override
    public void visit(OpBGP op) {
        counter += ((OpBGP)op).getPattern().size();
    }

    // Getter
    public int getTripleCount() {
        return counter;
    }

}
