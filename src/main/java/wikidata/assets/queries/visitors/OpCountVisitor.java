package wikidata.assets.queries.visitors;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.*;
import java.util.*;
import java.lang.Boolean;

// Jena ARQ
import org.apache.jena.query.*;
import org.apache.jena.sparql.algebra.*;
import org.apache.jena.sparql.algebra.op.*;

// Counts how often each op occurs
public class OpCountVisitor extends OpVisitorBase {

    // A map from each op to a number indicating how many there are.
    public Map<String, Integer> opCount = new HashMap<String, Integer>();

    public Map<String, Integer> getOpCount() {
        return opCount;
    }

    // Addes a given op to the opCounter
    public void addOp(Op op) {
        String name = op.getName();
        Integer count = opCount.get(name);
        if (count == null) opCount.put(name,0);
        count = opCount.get(name);
        opCount.put(name, count+1);
    }

    @Override
    public void visit(OpAssign op) {
        addOp(op);
    }

    @Override
    public void visit(OpBGP op) {
        addOp(op);
    }

    @Override
    public void visit(OpConditional op) {
        addOp(op);
    }

    @Override
    public void visit(OpDatasetNames op) {
        addOp(op);
    }

    @Override
    public void visit(OpDiff op) {
        addOp(op);
    }

    @Override
    public void visit(OpDisjunction op) {
        addOp(op);
    }

    @Override
    public void visit(OpDistinct op) {
        addOp(op);
    }

    @Override
    public void visit(OpExtend op) {
        addOp(op);
    }

    @Override
    public void visit(OpFilter op) {
        addOp(op);
    }

    @Override
    public void visit(OpGraph op) {
        addOp(op);
    }

    @Override
    public void visit(OpGroup op) {
        addOp(op);
    }

    @Override
    public void visit(OpJoin op) {
        addOp(op);
    }

    @Override
    public void visit(OpLabel op) {
        addOp(op);
    }

    @Override
    public void visit(OpLeftJoin op) {
        addOp(op);
    }

    @Override
    public void visit(OpList op) {
        addOp(op);
    }

    @Override
    public void visit(OpMinus op) {
        addOp(op);
    }

    @Override
    public void visit(OpNull op) {
        addOp(op);
    }

    @Override
    public void visit(OpOrder op) {
        addOp(op);
    }

    @Override
    public void visit(OpPath op) {
        addOp(op);
    }

    @Override
    public void visit(OpProcedure op) {
        addOp(op);
    }

    @Override
    public void visit(OpProject op) {
        addOp(op);
    }

    @Override
    public void visit(OpPropFunc op) {
        addOp(op);
    }

    @Override
    public void visit(OpQuad op) {
        addOp(op);
    }

    @Override
    public void visit(OpQuadBlock op) {
        addOp(op);
    }

    @Override
    public void visit(OpQuadPattern op) {
        addOp(op);
    }

    @Override
    public void visit(OpReduced op) {
        addOp(op);
    }

    @Override
    public void visit(OpSequence op) {
        addOp(op);
    }

    @Override
    public void visit(OpService op) {
        addOp(op);
    }

    @Override
    public void visit(OpSlice op) {
        addOp(op);
    }

    @Override
    public void visit(OpTable op) {
        addOp(op);
    }

    @Override
    public void visit(OpTopN op) {
        addOp(op);
    }

    @Override
    public void visit(OpTriple op) {
        addOp(op);
    }

    @Override
    public void visit(OpUnion op) {
        addOp(op);
    }

}
