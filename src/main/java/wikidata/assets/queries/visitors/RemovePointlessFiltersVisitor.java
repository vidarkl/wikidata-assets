package wikidata.assets.queries.visitors;

import java.util.*;

// Jena ARQ
import org.apache.jena.query.*;
import org.apache.jena.sparql.algebra.*;
import org.apache.jena.sparql.algebra.op.*;
import org.apache.jena.sparql.expr.*;

import wikidata.assets.queries.transformers.SourceTargetTransformer;

// A filter with a variable not existing in the set of triples is pointless
// Remove such filters
// This visitor is given a op and a set of pointless variables.
// getFixedOp returns the new fixed op witout the pointless filters
public class RemovePointlessFiltersVisitor extends OpVisitorBase {

    Set<String> pointlessVariables;
    Op mainOp;
    Op fixedOp;

    public Op getFixedOp() {
        return this.fixedOp;
    }

    // Constructor. Set the pointlessVariables
    public RemovePointlessFiltersVisitor(Op mainOp, Set<String> pointlessVariables) {
        this.pointlessVariables = pointlessVariables;
        this.mainOp = mainOp;
    }

    @Override
    public void visit(OpBGP op) {
    }


    @Override
    public void visit(OpFilter op) {
        this.fixedOp = this.mainOp;

        Op subOp = op.getSubOp();
        ExprList exps = op.getExprs();
        ExprList newExpressions = new ExprList();  // add new expressions here
        for (Expr expr : exps) {
            Expr leftSide = ((ExprFunction2)expr).getArg1();
            Expr rightSide = ((ExprFunction2)expr).getArg2();

            ExprVar variable = null;

            if (leftSide instanceof ExprVar) {
                variable = (ExprVar)leftSide;
            }
            if (rightSide instanceof ExprVar) {
                variable = (ExprVar)rightSide;
            }
            String varName = variable.getVarName();
            if (!(pointlessVariables.contains(varName))) {
                newExpressions.add(expr);
            }
        }

        if (newExpressions.size() > 0) {
            OpFilter newOp =  OpFilter.filterAlways(newExpressions, subOp);
            SourceTargetTransformer trans = new SourceTargetTransformer(op, newOp);
            Op newMainOp = Transformer.transform(trans, mainOp);
            this.fixedOp = newMainOp;
        }
        else {
            SourceTargetTransformer trans = new SourceTargetTransformer(op, subOp);
            Op newMainOp = Transformer.transform(trans, mainOp);
            this.fixedOp = newMainOp;
        }

    }
}
