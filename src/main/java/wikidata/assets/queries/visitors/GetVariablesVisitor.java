package wikidata.assets.queries.visitors;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.*;
import java.util.*;
import java.lang.Boolean;

// Jena ARQ
import org.apache.jena.query.*;
import org.apache.jena.sparql.algebra.*;
import org.apache.jena.sparql.algebra.op.*;

import org.apache.jena.sparql.core.*;
import org.apache.jena.sparql.path.*;
import org.apache.jena.graph.Triple;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;

import org.apache.jena.sparql.core.BasicPattern;
import org.apache.jena.graph.Triple;

import org.apache.jena.sparql.core.*;
import org.apache.jena.sparql.path.*;
import org.apache.jena.sparql.expr.*;
import org.apache.jena.sparql.expr.nodevalue.*;


// Visitor to collect all the variables used in an op
// This can only be used in an op which is has been simplified to only triples and filters
// It will give a set of variables used in filters, and a set of variables used in triples
public class GetVariablesVisitor extends OpVisitorBase {

    private Set<String> filterVariables = new HashSet<String>();
    private Set<String> tripleVariables = new HashSet<String>();

    // Get the name of all variables used in filters
    public Set<String> getFilterVariables() {
        return filterVariables;
    }

    // Get the name of all variables used in triples
    public Set<String> getTripleVariables() {
        return tripleVariables;
    }



    @Override
    public void visit(OpBGP opBGP) {
        BasicPattern gp = opBGP.getPattern();
        for (Triple t : gp) {

            Node s = t.getSubject();
            if (s.isVariable()) { tripleVariables.add(s.getName()); }

            Node p = t.getPredicate();
            if (p.isVariable()) { tripleVariables.add(p.getName()); }

            Node o = t.getObject();
            if (o.isVariable()) { tripleVariables.add(o.getName()); }
        }
    }

    @Override
    public void visit(OpFilter op) {
        ExprList exps = op.getExprs();
        for (Expr expr : exps) {
            Expr leftSide = ((ExprFunction2)expr).getArg1();
            Expr rightSide = ((ExprFunction2)expr).getArg2();

            if (leftSide instanceof ExprVar) {
                filterVariables.add(((ExprVar)leftSide).getVarName());
            }
            if (rightSide instanceof ExprVar) {
                filterVariables.add(((ExprVar)rightSide).getVarName());
            }
        }
    }

}
