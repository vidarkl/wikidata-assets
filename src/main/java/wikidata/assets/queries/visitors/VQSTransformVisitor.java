package wikidata.assets.queries.visitors;


import java.util.*;

// Jena ARQ
import org.apache.jena.query.*;
import org.apache.jena.sparql.algebra.*;
import org.apache.jena.sparql.algebra.op.*;

import org.apache.jena.sparql.core.*;
import org.apache.jena.sparql.path.*;

import wikidata.assets.queries.transformers.SourceTargetTransformer;

import org.apache.jena.graph.Triple;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;

import org.apache.jena.sparql.core.*;
import org.apache.jena.sparql.path.*;
import org.apache.jena.sparql.expr.*;
import org.apache.jena.sparql.expr.nodevalue.*;


// This visitor takes any query and transforms it into multiple new queries only consisting of filters and bgp triples
public class VQSTransformVisitor extends OpVisitorBase {

    private List<Op> ops = new ArrayList<Op>();  // The list of ops to return each round
    private Boolean stopVisiting = false;  // If one transform has been done, we need to stop. 
    private Op mainOp;  // The main op we are visiting over at the moment


    // Constructor
    public VQSTransformVisitor(Op mainOp) {
        this.mainOp = mainOp;
    }

    // Getter
    public List<Op> getOps() {
        return ops;
    }


    // This method is called when the op should be replaced by its subop. I.e. Just ignore the op. This can be done for all ops of type Op1
    public void replaceWithSubOp(Op1 op) {
        if (!stopVisiting) {
            stopVisiting = true;
            Op subOp = op.getSubOp();

            // Tell the transformer to transform the mainOp: op -> subOp
            SourceTargetTransformer trans = new SourceTargetTransformer(op, subOp);
            Op newOp = Transformer.transform(trans, mainOp);

            ops.add(newOp);
        }
    }

    // Keep only a subset of the existing filters
    @Override
    public void visit (OpFilter op) {
        if (!stopVisiting) {
            stopVisiting = true;

            Op subOp = op.getSubOp();
            ExprList exps = op.getExprs();
            if (exps.size() == 0) {
                // If the filter contains no expressions, we remove it
                SourceTargetTransformer trans = new SourceTargetTransformer(op, subOp);
                Op newMainOp = Transformer.transform(trans, mainOp);
                ops.add(newMainOp);
            }
            else {
                // Only keep simple filters: only variables compared to values
                ExprList newExpressions = new ExprList();
                boolean expressionListChanged = false;
                for (Expr expr : exps) {
                    boolean expressionAccepted = false;
                    // Accepting equals and less/greater
                    if (expr instanceof E_GreaterThan || expr instanceof E_LessThan || expr instanceof E_GreaterThanOrEqual || expr instanceof E_LessThanOrEqual || expr instanceof E_Equals) {
                        Expr leftSide = ((ExprFunction2)expr).getArg1();
                        Expr rightSide = ((ExprFunction2)expr).getArg2();

                        if ((leftSide instanceof ExprVar && rightSide instanceof NodeValue) || (leftSide instanceof NodeValue && rightSide instanceof ExprVar)) {
                            newExpressions.add(expr);
                            expressionAccepted = true;
                        }
                    }
                    if (!expressionAccepted) expressionListChanged = true;
                }

                // Only if the list of expressions changes we will add the new op
                if (expressionListChanged) {
                    // Add newexpressions to make new op
                    OpFilter newOp =  OpFilter.filterAlways(newExpressions, subOp);

                    SourceTargetTransformer trans = new SourceTargetTransformer(op, newOp);
                    Op newMainOp = Transformer.transform(trans, mainOp);
                    ops.add(newMainOp);
                }
            }
        }
    }


    @Override
    public void visit(OpProject op) {
        replaceWithSubOp(op);
    }

    @Override
    public void visit(OpGroup op) {
        replaceWithSubOp(op);
    }

    @Override
    public void visit(OpSlice op) {
        replaceWithSubOp(op);
    }

    @Override
    public void visit(OpOrder op) {
        replaceWithSubOp(op);
    }

    @Override
    public void visit(OpDistinct op) {
        replaceWithSubOp(op);
    }

    @Override
    public void visit(OpExtend op) {
        replaceWithSubOp(op);
    }


    // The paths are hard to deal with.
    // Transforming p1/p2 into two new triples
    // Also translating p1/(p2)* into p1, p1-p2, and p1-p2-p2
    // TODO maybe: Do p1|p2
    @Override
    public void visit(OpPath op) {
        if (!stopVisiting) {
            stopVisiting = true;

            // First get the triplepath. This is either a triple or a path
            TriplePath tp = op.getTriplePath();

            // Get the path part of the triple
            Path p = tp.getPath();

            if (p instanceof  P_Seq) { 

                Path left = ((P_Seq)p).getLeft();
                Path right = ((P_Seq)p).getRight();

                 // Now I only care about the p1/p2
                if (left instanceof P_Link && right instanceof P_Link) {
                    // Translate the two sides to P-links
                    Node l = ((P_Link)left).getNode();
                    Node r = ((P_Link)right).getNode();

                    BasicPattern bp = new BasicPattern();

                    Node subj = tp.getSubject();
                    Node obj = tp.getObject();

                    // Create blank node and make the two new triples
                    String uniqueID = UUID.randomUUID().toString().replace("-", "_");  // Create a unique id to use in the variable.
                    Node inBetweenVariableNode = NodeFactory.createVariable("var_temp_" + uniqueID);
                    Triple triple1 = new Triple(subj, l, inBetweenVariableNode);
                    Triple triple2 = new Triple(inBetweenVariableNode, r, obj);

                    // Add triples to basicpattern
                    bp.add(triple1);
                    bp.add(triple2);

                    OpBGP opBGP = new OpBGP(bp);

                    // Tell the transformer to transform the mainOp: op -> subOp
                    SourceTargetTransformer trans = new SourceTargetTransformer(op, opBGP);
                    Op newOp = Transformer.transform(trans, mainOp);

                    ops.add(newOp);
                }
                // case with p1/(p2)* - turn into 3 cases with 0, 1 and 2 usages of the right property
                if (left instanceof P_Link && right instanceof P_ZeroOrMore1) {
                    Node l = ((P_Link)left).getNode();

                    // This work always
                    P_ZeroOrMore1 zeroOrMoreRight = (P_ZeroOrMore1)right;

                    Path s = zeroOrMoreRight.getSubPath();
                    if (s instanceof P_Link) {

                        Node r = ((P_Link)s).getNode();

                        Node subj = tp.getSubject();
                        Node obj = tp.getObject();

                        List<BasicPattern> basicPatterns = new ArrayList<BasicPattern>();

                        // 0. Just skip the right part
                        BasicPattern bp0 = new BasicPattern();
                        Triple triple0 = new Triple(subj, l, obj);
                        bp0.add(triple0);
                        basicPatterns.add(bp0);

                        // 1. Use right 1 time
                        BasicPattern bp1 = new BasicPattern();
                        String uniqueID1 = UUID.randomUUID().toString().replace("-", "_");  // Create a unique id to use in the variable.
                        Node inBetweenVariableNode1 = NodeFactory.createVariable("var_temp_" + uniqueID1);
                        Triple triple11 = new Triple(subj, l, inBetweenVariableNode1);
                        Triple triple12 = new Triple(inBetweenVariableNode1, r, obj);
                        bp1.add(triple11);
                        bp1.add(triple12);
                        basicPatterns.add(bp1);

                        // 2. Use right 2 times
                        BasicPattern bp2 = new BasicPattern();
                        String uniqueID21 = UUID.randomUUID().toString().replace("-", "_");  // Create a unique id to use in the variable.
                        String uniqueID22 = UUID.randomUUID().toString().replace("-", "_");  // Create a unique id to use in the variable.
                        Node inBetweenVariableNode21 = NodeFactory.createVariable("var_temp_" + uniqueID21);
                        Node inBetweenVariableNode22 = NodeFactory.createVariable("var_temp_" + uniqueID22);
                        Triple triple21 = new Triple(subj, l, inBetweenVariableNode21);
                        Triple triple22 = new Triple(inBetweenVariableNode21, r, inBetweenVariableNode22);
                        Triple triple23 = new Triple(inBetweenVariableNode22, r, obj);
                        bp2.add(triple21);
                        bp2.add(triple22);
                        bp2.add(triple23);
                        basicPatterns.add(bp2);

                        // For each basic pattern: create a new op
                        for (BasicPattern bp : basicPatterns) {
                            OpBGP opBGP = new OpBGP(bp);
                            SourceTargetTransformer trans = new SourceTargetTransformer(op, opBGP);
                            Op newOp = Transformer.transform(trans, mainOp); // clean query
                            ops.add(newOp);
                        }
                    }
                }
            }
        }
    }


    // A sequence is like a join, just with many elements and with some extra condition.
    // Turn sequence into a join of first element and a new sequence with remaining elements.
    // If sequence only holds one element, remove the sequence and just use the element
    @Override
    public void visit(OpSequence op) {
        if (!stopVisiting) {
            stopVisiting = true;
            List<Op> originalElements = ((OpN)op).getElements();

            if (originalElements.size() == 1) {
                // When the sequence is only one item, it is not really a sequence anymore, so just replace with the only element
                Op opFirstElement = originalElements.get(0);
                SourceTargetTransformer trans = new SourceTargetTransformer(op, opFirstElement);
                Op newOp = Transformer.transform(trans, mainOp);
                ops.add(newOp);
            }
            else {
                // replace with a join between the first element and a new sequence of the remaining ones
                Op firstElement = originalElements.get(0);
                OpSequence newSequence = OpSequence.create();
                for (int i = 1 ; i < originalElements.size(); i++) {
                    Op el = originalElements.get(i);
                    newSequence.add(el);
                }

                // Make join from first element and the remaining sequence
                Op opJoin = OpJoin.create(firstElement, newSequence);

                SourceTargetTransformer trans = new SourceTargetTransformer(op, opJoin);
                Op newOp = Transformer.transform(trans, mainOp);
                ops.add(newOp);
            }

        }

    }



    @Override
    public void visit(OpService op) {
        if (!stopVisiting) {
            stopVisiting = true;
            Op emptyBGP = new OpBGP();

            SourceTargetTransformer trans = new SourceTargetTransformer(op, emptyBGP);
            Op newOp = Transformer.transform(trans, mainOp); // clean query
            ops.add(newOp);

        }
    }

    @Override
    public void visit(OpLeftJoin op) {
        if (!stopVisiting) {
            stopVisiting = true;
            Op opLeft = op.getLeft();
            Op opRight = op.getRight();

            // Tell the transformer to transform the mainOp: op -> subOp
            SourceTargetTransformer trans1 = new SourceTargetTransformer(op, opLeft);
            Op newOp1 = Transformer.transform(trans1, mainOp); // clean query
            ops.add(newOp1);

            // Tell the transformer to something else
            Op opJoin = OpJoin.create(opLeft, opRight);
            SourceTargetTransformer trans2 = new SourceTargetTransformer(op, opJoin);
            Op newOp2 = Transformer.transform(trans2, mainOp); // clean query
            ops.add(newOp2);

        }
    }


    // Tanslate from one join of two bgp 
    @Override
    public void visit(OpJoin op) {
        if (!stopVisiting) {
            stopVisiting = true;
            Op opLeft = op.getLeft();
            Op opRight = op.getRight();

            if (opLeft instanceof OpBGP && opRight instanceof OpBGP) {
                BasicPattern bp = new BasicPattern();

                // Add the triples
                for (Triple t : ((OpBGP)opLeft).getPattern()) { bp.add(t); }
                for (Triple t : ((OpBGP)opRight).getPattern()) { bp.add(t); }

                OpBGP newBGP = new OpBGP(bp);

                // Tell the transformer to transform the mainOp: op -> newBGP
                SourceTargetTransformer trans = new SourceTargetTransformer(op, newBGP);
                Op newOp = Transformer.transform(trans, mainOp); // clean query
                ops.add(newOp);

            }
        }
    }

    // If union, then replace it with two verions, one for left and right of the union
    @Override
    public void visit(OpMinus op) {
        if (!stopVisiting) {
            stopVisiting = true;
            Op opLeft = op.getLeft();
            Op opRight = op.getRight();

            // Tell the transformer to transform the mainOp: op -> subOp
            SourceTargetTransformer trans1 = new SourceTargetTransformer(op, opLeft);
            Op newOp1 = Transformer.transform(trans1, mainOp); // clean query
            ops.add(newOp1);

            // Tell the transformer to transform the mainOp: op -> subOp
            SourceTargetTransformer trans2 = new SourceTargetTransformer(op, opRight);
            Op newOp2 = Transformer.transform(trans2, mainOp); // clean query
            ops.add(newOp2);
        }
    }


    // If union, then replace it with two verions, one for left and right of the union
    @Override
    public void visit(OpUnion op) {
        if (!stopVisiting) {
            stopVisiting = true;
            Op opLeft = op.getLeft();
            Op opRight = op.getRight();

            // Tell the transformer to transform the mainOp: op -> subOp
            SourceTargetTransformer trans1 = new SourceTargetTransformer(op, opLeft);
            Op newOp1 = Transformer.transform(trans1, mainOp); // clean query
            ops.add(newOp1);

            // Tell the transformer to transform the mainOp: op -> subOp
            SourceTargetTransformer trans2 = new SourceTargetTransformer(op, opRight);
            Op newOp2 = Transformer.transform(trans2, mainOp); // clean query
            ops.add(newOp2);
        }
    }

}
