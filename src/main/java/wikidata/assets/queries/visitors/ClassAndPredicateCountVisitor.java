package wikidata.assets.queries.visitors;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.*;
import java.util.*;
import java.lang.Boolean;

// Jena ARQ
import org.apache.jena.query.*;
import org.apache.jena.sparql.algebra.*;
import org.apache.jena.sparql.algebra.op.*;

import org.apache.jena.sparql.core.BasicPattern;

import wikidata.assets.queries.Utils;

import org.apache.jena.graph.Triple;
import org.apache.jena.graph.Node;


public class ClassAndPredicateCountVisitor extends OpVisitorBase {

    // Contains count of how often each uri appears in the class position.
    Map<String, Integer> classCount = new HashMap<String, Integer>();
    Map<String, Integer> propertyCount = new HashMap<String, Integer>();

    public Map<String, Integer> getClassCount() {
        return classCount;
    }

    public Map<String, Integer> getPropertyCount() {
        return propertyCount;
    }

    public void count(Op op) {
        if (op instanceof OpBGP ) {
            BasicPattern bp = ((OpBGP)op).getPattern();
            for (Triple triple : bp) {
                Node subject = triple.getSubject();
                Node predicate = triple.getPredicate();
                Node object = triple.getObject();

                // Count the type
                if (subject.isVariable() && predicate.isURI() && object.isURI()) {
                    if (Utils.getTypePredicates().contains(predicate.getURI())) {
                        String uri = object.getURI();

                        Integer c = classCount.get(uri);
                        if (c == null) classCount.put(uri, 0);
                        c = classCount.get(uri);
                        classCount.put(uri, c+1);
                    }
                }

                // Count the predicate
                if (predicate.isURI()) {
                    String uri = predicate.getURI();
                    Integer c = propertyCount.get(uri);
                    if (c == null) propertyCount.put(uri, 0);
                    c = propertyCount.get(uri);
                    propertyCount.put(uri, c+1);
                }
            }
        }
    }

    @Override
    public void visit(OpAssign op) {
        count(op);
    }

    @Override
    public void visit(OpBGP op) {
        count(op);
    }

    @Override
    public void visit(OpConditional op) {
        count(op);
    }

    @Override
    public void visit(OpDatasetNames op) {
        count(op);
    }

    @Override
    public void visit(OpDiff op) {
        count(op);
    }

    @Override
    public void visit(OpDisjunction op) {
        count(op);
    }

    @Override
    public void visit(OpDistinct op) {
        count(op);
    }

    @Override
    public void visit(OpExtend op) {
        count(op);
    }

    @Override
    public void visit(OpFilter op) {
        count(op);
    }

    @Override
    public void visit(OpGraph op) {
        count(op);
    }

    @Override
    public void visit(OpGroup op) {
        count(op);
    }

    @Override
    public void visit(OpJoin op) {
        count(op);
    }

    @Override
    public void visit(OpLabel op) {
        count(op);
    }

    @Override
    public void visit(OpLeftJoin op) {
        count(op);
    }

    @Override
    public void visit(OpList op) {
        count(op);
    }

    @Override
    public void visit(OpMinus op) {
        count(op);
    }

    @Override
    public void visit(OpNull op) {
        count(op);
    }

    @Override
    public void visit(OpOrder op) {
        count(op);
    }

    @Override
    public void visit(OpPath op) {
        count(op);
    }

    @Override
    public void visit(OpProcedure op) {
        count(op);
    }

    @Override
    public void visit(OpProject op) {
        count(op);
    }

    @Override
    public void visit(OpPropFunc op) {
        count(op);
    }

    @Override
    public void visit(OpQuad op) {
        count(op);
    }

    @Override
    public void visit(OpQuadBlock op) {
        count(op);
    }

    @Override
    public void visit(OpQuadPattern op) {
        count(op);
    }

    @Override
    public void visit(OpReduced op) {
        count(op);
    }

    @Override
    public void visit(OpSequence op) {
        count(op);
    }

    @Override
    public void visit(OpService op) {
        count(op);
    }

    @Override
    public void visit(OpSlice op) {
        count(op);
    }

    @Override
    public void visit(OpTable op) {
        count(op);
    }

    @Override
    public void visit(OpTopN op) {
        count(op);
    }

    @Override
    public void visit(OpTriple op) {
        count(op);
    }

    @Override
    public void visit(OpUnion op) {
        count(op);
    }

}
