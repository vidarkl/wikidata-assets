package wikidata.assets.queries;

import org.apache.jena.sparql.algebra.*;

// This OpWeight class combines and Op with a given weight.
// It is used when working with the query catalogue
public class OpWeight {
    Op op;
    Double weight;

    // Constructor
    OpWeight(Op op, Double weight) {
        this.op = op;
        this.weight = weight;
    }
}
