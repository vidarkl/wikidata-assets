package wikidata.assets.queries.transformers;

// Java Util
import java.util.*;

// Jena ARQ
import org.apache.jena.query.*;
import org.apache.jena.sparql.algebra.*;
import org.apache.jena.sparql.algebra.op.*;
import org.apache.jena.sparql.core.*;

import wikidata.assets.queries.Utils;

import org.apache.jena.graph.*;


// Transformer used to create a new version of a query where the variable types are given in a map m
// The map m is a map<String, String> from variables to types.
public class AddTypesByMapTransformer extends TransformCopy {

    Map<String, String> variableTypeMap;

    public AddTypesByMapTransformer(Map<String, String> m) {
        this.variableTypeMap = m;
    }

    @Override
    public Op transform(OpBGP opBGP) {
        BasicPattern pattern = opBGP.getPattern();
        BasicPattern newPattern = new BasicPattern();
        for (Triple t: pattern) {
            Node s = t.getSubject();
            Node p = t.getPredicate();
            Node o = t.getObject();

            // If a type triple, skip it, since the types are going to be defined by the map instead
            List<String> typeTriples = Utils.getTypePredicates();
            if (!typeTriples.contains(p.getURI())) {
                newPattern.add(t);
            }
        }

        // Add types from the map
        for (Map.Entry<String, String> entry : variableTypeMap.entrySet()) {
            Node variableNode = NodeFactory.createVariable(entry.getKey());
            Node predicateNode = NodeFactory.createURI(Utils.getRdfTypePredicate());
            Node typeNode = NodeFactory.createURI(entry.getValue());
            newPattern.add(new Triple(variableNode, predicateNode, typeNode));
        }

        Op newOpBGP = new OpBGP(newPattern);
        return newOpBGP;
    }
}
 