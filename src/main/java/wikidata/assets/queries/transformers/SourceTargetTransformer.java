package wikidata.assets.queries.transformers;

// Java
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.*;
import java.util.*;

// Jena ARQ
import org.apache.jena.query.*;
import org.apache.jena.sparql.algebra.*;
import org.apache.jena.sparql.algebra.op.*;


// This is a transformer that replaces every occurence of source with target in the visited op
// For it to work we need to set the  source and target first. This is done in the constructor.
// It only works for the op types given below. 
public class SourceTargetTransformer extends TransformCopy {

    Op source;  // The source op
    Op target;  // The target op

    // Constructor
    public SourceTargetTransformer(Op source, Op target) {
        this.source = source;
        this.target = target;
    }

    @Override
    public Op transform(OpPath op)  {
        if (op.equals(source)) return target;
        return super.transform(op);
    }

    @Override
    public Op transform(OpFilter op, Op subOp)  {
        if (op.equals(source)) return target;
        return super.transform(op, subOp);
    }

    @Override
    public Op transform(OpJoin op, Op opLeft, Op opRight) {
        if (op.equals(source)) return target;
        return super.transform(op, opLeft, opRight);
    }


    @Override
    public Op transform(OpSequence op, List<Op> elts) {
        if (op.equals(source)) return target;
        return super.transform(op, elts);
    }

    @Override
    public Op transform(OpProject op, Op subOp) {
        if (op.equals(source)) return target;
        return super.transform(op, subOp);
    }

    @Override
    public Op transform(OpExtend op, Op subOp) {
        if (op.equals(source)) return target;
        return super.transform(op, subOp);
    }

    @Override
    public Op transform(OpGroup op, Op subOp) {
        if (op.equals(source)) return target;
        return super.transform(op, subOp);
    }

    @Override
    public Op transform(OpOrder op, Op subOp) {
        if (op.equals(source)) return target;
        return super.transform(op, subOp);
    }

    @Override
    public Op transform(OpDistinct op, Op subOp) {
        if (op.equals(source)) return target;
        return super.transform(op, subOp);
    }

    @Override
    public Op transform(OpSlice op, Op subOp) {
        if (op.equals(source)) return target;
        return super.transform(op, subOp);
    }

    @Override
    public Op transform(OpLeftJoin op, Op left, Op right) {
        if (op.equals(source)) return target;
        return super.transform(op, left, right);
    }

    @Override
    public Op transform(OpMinus op, Op left, Op right) {
        if (op.equals(source)) return target;
        return super.transform(op, left, right);
    }

    @Override
    public Op transform(OpUnion op, Op left, Op right) {
        if (op.equals(source)) return target;
        return super.transform(op, left, right);
    }


    @Override
    public Op transform(OpService op, Op subOp) {
        if (op.equals(source)) return target;
        return super.transform(op, subOp);
    }

}
 