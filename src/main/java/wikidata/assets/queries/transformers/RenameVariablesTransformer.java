package wikidata.assets.queries.transformers;



// Misc Java
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.*;
import java.util.*;

// Jena ARQ
import org.apache.jena.query.*;
import org.apache.jena.sparql.algebra.*;
import org.apache.jena.sparql.algebra.op.*;
import org.apache.jena.sparql.core.*;
import org.apache.jena.graph.*;


// This transformer is initiated with a map from source variables names to target variable names.
// It transforms the op into a new op where all variable names are transformed according to the map
// Renaming is not done for the filters, only the triples in the main basic graph pattern
public class RenameVariablesTransformer extends TransformCopy {

    Map<String, String> variableTransformMap;

    // Constructor
    public RenameVariablesTransformer(Map<String, String> variableTransformMap) {
        this.variableTransformMap = variableTransformMap;
    }

    @Override
    public Op transform(OpBGP opBGP) {
        BasicPattern pattern = opBGP.getPattern();
        BasicPattern newPattern = new BasicPattern();
        for (Triple t: pattern) {
            Node s = t.getSubject();
            Node p = t.getPredicate();
            Node o = t.getObject();

            if (s.isVariable()) { s = NodeFactory.createVariable(variableTransformMap.get(s.getName())); }
            Node newPredicate = p;
            if (o.isVariable()) { 
                o = NodeFactory.createVariable(variableTransformMap.get(o.getName())); 
            }

            Triple newTriple = new Triple(s, p, o);
            newPattern.add(newTriple);
        }

        return new OpBGP(newPattern);
    }
}
 