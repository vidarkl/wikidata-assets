package wikidata.assets.queries.transformers;

import java.util.*;

// Jena ARQ
import org.apache.jena.query.*;
import org.apache.jena.sparql.algebra.*;
import org.apache.jena.sparql.algebra.op.*;
import org.apache.jena.sparql.core.*;

import wikidata.assets.queries.Utils;

import org.apache.jena.graph.*;

// This transformer assumes that the query only contains simple filters and bgps with triples
// It fixes/removes invalid triples
// The predicate has to be a uri. 
// If the predicate is some kind of type, then the subject must be a variable, and the object must be a uri.
// Otherwise the triple must be (var, uri, var)
// If the form is (var, uri, uri) or (uri, uri, var), we will change to (var, uri, var). This generalizes the query and will give more answers.
public class EnsureVqsTriplesTransformer extends TransformCopy {


    @Override
    public Op transform(OpBGP opBGP) {
        BasicPattern pattern = opBGP.getPattern();
        BasicPattern newPattern = new BasicPattern();
        for (Triple t: pattern) {
            Node s = t.getSubject();
            Node p = t.getPredicate();
            Node o = t.getObject();

            // We only consider triples with uri predicates
            if (p.isURI()) {
                String predicateURI = p.getURI();

                List <String> typePredicates = Utils.getTypePredicates();
                if (typePredicates.contains(predicateURI)) {
                    // Case triple with typing, then the object must be a uri
                    // Also the predicate must be rdf:type
                    if (s.isVariable() && o.isURI() ) {
                        Node newPredicate = NodeFactory.createURI(Utils.getRdfTypePredicate());
                        Triple newTypeURITriple = new Triple(s, newPredicate, o);
                        newPattern.add(newTypeURITriple);
                    }
                }
                else {
                    // If already variable to variable, add it
                    if (s.isVariable() && o.isVariable()) {
                        newPattern.add(t);
                    }
                    // If only object is variable, make the subject variable as well.
                    if (!s.isVariable() && o.isVariable()) {
                        String uniqueID = UUID.randomUUID().toString().replace("-", "_");  // Create a unique id to use in the variable.
                        Node tempVar = NodeFactory.createVariable("var_temp_" + uniqueID);
                        Triple newTriple = new Triple(tempVar, p, o);
                        newPattern.add(newTriple);
                    }
                    // If only subject is variable, make the object variable as well.
                    if (s.isVariable() && !o.isVariable()) {
                        String uniqueID = UUID.randomUUID().toString().replace("-", "_");  // Create a unique id to use in the variable.
                        Node tempVar = NodeFactory.createVariable("var_temp_" + uniqueID);
                        Triple newTriple = new Triple(s, p, tempVar);
                        newPattern.add(newTriple);
                    }
                    // If none of them are variables, discard the whole triple
                    if (!s.isVariable() && !o.isVariable()) {
                        continue;
                    }
                }
            }
        }
        Op newOpBGP = new OpBGP(newPattern);
        return newOpBGP;
    }
}
 