package wikidata.assets.queries.transformers;


// Java Util
import java.util.*;

// Jena ARQ
import org.apache.jena.query.*;
import org.apache.jena.sparql.algebra.*;
import org.apache.jena.sparql.algebra.op.*;
import org.apache.jena.sparql.core.*;

import wikidata.assets.queries.Utils;

import org.apache.jena.graph.*;


// Used when filtering triples by navigation graph
// This filtering is very primitive. It only considers a list of allowed predicates and allowed types.
// It removes triples not fitting this model.
public class FilterTriplesTransformer extends TransformCopy {

    List<String> allowedPredicates;
    List<String> allowedClasses;

    public FilterTriplesTransformer(List<String> allowedClasses, List<String> allowedPredicates) {
        this.allowedClasses = allowedClasses;
        this.allowedPredicates = allowedPredicates;
    }

    @Override
    public Op transform(OpBGP opBGP) {
        BasicPattern pattern = opBGP.getPattern();
        List<Triple> newPatternList = new ArrayList<Triple>();
        for (Triple t: pattern) {
            Node s = t.getSubject();
            Node p = t.getPredicate();
            Node o = t.getObject();

            // In the triples we are given each of them has a  URI predicate
            String predicateURI = p.getURI();

            List <String> typePredicates = Utils.getTypePredicates();
            if (typePredicates.contains(predicateURI)) {
                String objectURI = o.getURI();
                if (allowedClasses.contains(objectURI)) {
                    newPatternList.add(t);
                }
            }
            else {
                // If already variable to variable, add it
                if (allowedPredicates.contains(predicateURI)) {
                    newPatternList.add(t);
                }
            }
        }


        BasicPattern newPattern = new BasicPattern();
        // Make sure that no triples are repeated:
        for (int i1 = 0 ; i1 < newPatternList.size() ; i1++) {
            Triple t1 = newPatternList.get(i1);
            Boolean foundDuplicateTriple = false;
            for (int i2 = 0 ; i2 < i1 ; i2++) {
                Triple t2 = newPatternList.get(i2);
                if (t1.getSubject().equals(t2.getSubject()) && t1.getPredicate().equals(t2.getPredicate()) && t1.getObject().equals(t2.getObject()) ) {
                    foundDuplicateTriple = true;
                }
            }

            if (!(foundDuplicateTriple)) {
                newPattern.add(t1);
            }
        }


        Op newOpBGP = new OpBGP(newPattern);
        return newOpBGP;
    }
}
 