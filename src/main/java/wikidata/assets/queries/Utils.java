package wikidata.assets.queries;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

// Misc java
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.*;
import java.util.*;

// Jena ARQ
import org.apache.jena.query.*;
import org.apache.jena.sparql.algebra.*;
import org.apache.jena.sparql.algebra.op.*;

// Jgrapht
import org.jgrapht.*;
import org.jgrapht.graph.*;
import org.jgrapht.traverse.*;

import wikidata.assets.queries.transformers.RenameVariablesTransformer;
import wikidata.assets.queries.visitors.ClassAndPredicateCountVisitor;
import wikidata.assets.queries.visitors.GetVariablesVisitor;
import wikidata.assets.queries.visitors.OpCountVisitor;
import wikidata.assets.queries.visitors.RemovePointlessFiltersVisitor;
import wikidata.assets.queries.visitors.TripleCountVisitor;

// This util class contains a set of useful functions which may be used by many other classes
public class Utils {


    public static String getRdfTypePredicate() {
        return "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
    }


    // Take a list of ops with weight, and print them to file with the given outputFileName
    public static void writeOpsToFile(List<OpWeight> ops, String outputFileName) {
        try {
            FileWriter fw = new FileWriter(outputFileName);
            for (OpWeight ow : ops) {
                Query q = OpAsQuery.asQuery(ow.op);
                String queryString = q.toString().replace("\n", " ").trim().replaceAll(" +", " "); // Get a nice one-liner query
                fw.append(ow.weight + "\t" + queryString + "\n");
            }
            fw.close();
        }
        catch (Exception e){System.out.println("[ERROR] Failed while writing ops to file." + e);}
    }


    // This is where it is defined what a type predicate is. rdf:type and P31 are typical examples, but there may be more.
    public static List<String> getTypePredicates() {
        List<String> typePredicates = new ArrayList<String>();
        typePredicates.add("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
        typePredicates.add("http://www.wikidata.org/prop/direct/P31");
        typePredicates.add("a");
        return typePredicates;
    }



    // Parse one string into an op. The string must be a sparql query.
    public static Op parseQueryString(String queryString) {
        try {
            Query query = QueryFactory.create(queryString);  // Create query
            if (!query.isSelectType()) {
                System.out.println("Not a select query - return null");
                return null;  // If it is not a select query, do not parse it
            }
            Op op = Algebra.compile(query);
            OpAsQuery.asQuery(op).toString();  
            return op;
        }
        catch (Exception e){ System.out.println("[WARNING] Failed to create op for query." + e); return null; }
    }



    // Read a file with queries (one query per line) into a list of query strings. Read all lines
    public static List<String> readFileToList(String filePath) throws Exception {
        return readFileToList(filePath, null);  // Read lines but no restriction on number of lines
    }



    // Read each line of a file into a list of strings.
    // If n is not null, then only the n first lines will be included.
    public static List<String> readFileToList(String filePath, Integer n) throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(filePath));
        String line;
        List<String> returnList = new ArrayList<String>();
        while ((line = br.readLine()) != null) {
            returnList.add(line);
            if (n != null) {
                if (returnList.size() >= n) break;
            }
        }
        br.close();
        return returnList;
    }




    // This method combines four of he cleaning steps into one big cleaner method.
    // It is always safe to run this over a catalogue of queries.
    public static List<OpWeight> cleanOps(List <OpWeight> originalOps){
        // 1. RemoveEmpty 
        // Remove all ops with empty bgps. I.e empty queries
        List<OpWeight> removedEmptyOps = new ArrayList<OpWeight>();
        for (OpWeight ow : originalOps) {
            TripleCountVisitor tripleCountVisitor = new TripleCountVisitor();
            OpWalker.walk(ow.op, tripleCountVisitor);
            Integer tripleCount = tripleCountVisitor.getTripleCount();
            if (tripleCount > 0) {
                removedEmptyOps.add(ow);
            }
        }


        // 2. RenameTempVars
        // Rename temporary variables introduced by me earlier. This is to avoid long and stupid variable names
        List<OpWeight> renamedTempVarsOps = new ArrayList<OpWeight>();
        for (OpWeight ow : removedEmptyOps) {

            // Get a list of variables used in filters and in triples
            Set<String> variables = new HashSet<String>();
            GetVariablesVisitor vis = new GetVariablesVisitor();
            OpWalker.walk(ow.op, vis);
            variables.addAll(vis.getFilterVariables());
            variables.addAll(vis.getTripleVariables());


            // Make a map with source and target for each variable
            Map<String, String> variableTransformMap = new HashMap<String, String>();

            // When we now have the vars, we split into temp and not temp
            Set<String> tempVariables = new HashSet<String>();
            for (String variable : variables) {
                if (variable.contains("var_temp_")) { tempVariables.add(variable); }
                else { variableTransformMap.put(variable, variable); }
            }

            // If we have any temporary variables - find a map from old variables to new variables, and do the remapping
            int varIndex = 0;
            while (!(tempVariables.isEmpty())) {
                varIndex += 1;
                String variableString = "var" + varIndex;
                String value = variableTransformMap.get(variableString);
                if (value == null) {
                    for (String tempVariable : tempVariables) {
                        variableTransformMap.put(tempVariable, variableString);
                        tempVariables.remove(tempVariable);
                        break;
                    }
                }
            }

            // rename all variables according to the given map
            Op newOp = Transformer.transform(new RenameVariablesTransformer(variableTransformMap), ow.op);
            renamedTempVarsOps.add(new OpWeight(newOp, ow.weight));
        }


        // 3: In this step we remove filters which uses variables not present in triples. 
        // This is actually a problem, so we need to remove them
        List<OpWeight> removePointlessFiltersOps = new ArrayList<OpWeight>();
        for (OpWeight ow : renamedTempVarsOps) {

            // Get a list of variables used in filters and in triples
            GetVariablesVisitor getVarVisitor = new GetVariablesVisitor();
            OpWalker.walk(ow.op, getVarVisitor);
            Set<String> filterVariables = getVarVisitor.getFilterVariables();  
            Set<String> tripleVariables = getVarVisitor.getTripleVariables();

            // Find those only used in filters.
            Set<String> onlyFilterVariables = new HashSet<String>();
            for (String v : filterVariables) {
                if (!(tripleVariables.contains(v))) onlyFilterVariables.add(v);
            }

            // Only fix if needed
            if (onlyFilterVariables.size() > 0) {
                // Remove filters using variables from onlyFilterVariables
                // Use a visitor
                RemovePointlessFiltersVisitor removeFilterVisitor = new RemovePointlessFiltersVisitor(ow.op, onlyFilterVariables);
                OpWalker.walk(ow.op, removeFilterVisitor);
                Op fixedOp = removeFilterVisitor.getFixedOp();

                removePointlessFiltersOps.add(new OpWeight(fixedOp, ow.weight));
            }
            else {
                // Just do nothing, just add the op unchaged
                removePointlessFiltersOps.add(ow);
            }
        }


        // 4: Merge equal ops again, and add sum the weight.
        Map<Op, Double> opWeight = new LinkedHashMap<Op, Double>();
        for (OpWeight ow : removePointlessFiltersOps) {
            Double weightForOp = opWeight.getOrDefault(ow.op, 0.0);
            opWeight.put(ow.op, weightForOp + ow.weight);
        }

        List<OpWeight> mergedOps = new ArrayList<OpWeight>();
        for (Map.Entry<Op, Double> opEntry : opWeight.entrySet()) {
            Op op = opEntry.getKey();
            Double weight = opEntry.getValue();
            mergedOps.add(new OpWeight(op, weight));
        }

        return mergedOps;
    }









    // Print the statistics about a list of opWeigts.
    // Not really needed, but useful to get an overview of what is going on
    public static void printOpStats(List<OpWeight> queries) throws Exception {
        System.out.println("----------------------------STATISTICS-----------------------------------");
        System.out.println("Number of queries in this list of opweights: " + queries.size());
        Double totalSignificanceOverall = 0.0;
        for (OpWeight query : queries) {
            totalSignificanceOverall += query.weight;
        }
        System.out.println("Total significance (sum of weights) of this query list is " + totalSignificanceOverall);


        Map<String, Double> totalOpSignificance = new HashMap<String, Double>();
        Map<String, Double> totalClassSignificance = new HashMap<String, Double>();
        Map<String, Double> totalPredicateSignificance = new HashMap<String, Double>();
        Map<Integer, Double> totalTripleCount = new HashMap<Integer, Double>();

        for (OpWeight opWeight : queries) {
            Op op = opWeight.op;
            Double significance = opWeight.weight;

            // Count how much significance each op gets
            OpCountVisitor opCountVisitor = new OpCountVisitor();
            OpWalker.walk(op, opCountVisitor);
            Map<String, Integer> opCount = opCountVisitor.getOpCount();
            for (Map.Entry<String, Integer> entry : opCount.entrySet()) {
                String key = entry.getKey();
                Integer count = entry.getValue();
                Double currentSignificance = totalOpSignificance.getOrDefault(key, 0.0);
                totalOpSignificance.put(key, currentSignificance + Double.valueOf(count)*significance);
            }

            // Count classes and properties
            ClassAndPredicateCountVisitor classAndPredicateCountVisitor = new ClassAndPredicateCountVisitor();
            OpWalker.walk(op, classAndPredicateCountVisitor);

            // Count classes
            Map<String, Integer> classCount = classAndPredicateCountVisitor.getClassCount();
            for (Map.Entry<String, Integer> entry : classCount.entrySet()) {
                String key = entry.getKey();
                Integer count = entry.getValue();
                Double currentSignificance = totalClassSignificance.getOrDefault(key, 0.0);
                totalClassSignificance.put(key, currentSignificance + Double.valueOf(count)*significance);
            }

            // Count properties
            Map<String, Integer> propertyCount = classAndPredicateCountVisitor.getPropertyCount();
            for (Map.Entry<String, Integer> entry : propertyCount.entrySet()) {
                String key = entry.getKey();
                Integer count = entry.getValue();
                Double currentSignificance = totalPredicateSignificance.getOrDefault(key, 0.0);
                totalPredicateSignificance.put(key, currentSignificance + Double.valueOf(count)*significance);
            }

            // Count the number of triples
            TripleCountVisitor tripleCountVisitor = new TripleCountVisitor();
            OpWalker.walk(op, tripleCountVisitor);
            Integer tripleCount = tripleCountVisitor.getTripleCount();
            Double a = totalTripleCount.getOrDefault(tripleCount,0.0);
            totalTripleCount.put(tripleCount, significance+a);
        }



        System.out.println("\nNumber of triples:");
        // counting and displaying distribution of number of triples
        Double runningSignificanceCount = 0.0;
        for (int i = 0 ; i < 17 ; i++) {
            Double c = totalTripleCount.getOrDefault(i, 0.0);
            runningSignificanceCount += c;
            System.out.println( i + "\t" + String.format( "%.2f", c ) );
        }
        // Calculate the total significance over all queries
        Double totalSignificance = 0.0;
        for (OpWeight o : queries) {
            totalSignificance += o.weight;
        }
        Double rem = totalSignificance - runningSignificanceCount;
        if (rem < 0.000001) rem = 0.0;
        System.out.println("rest:" + "\t" + String.format( "%.2f", rem ) );



        // Print the ops
        System.out.println("\nTotal op significance:");
        totalOpSignificance.entrySet().stream().sorted((k1, k2) -> -k1.getValue().compareTo(k2.getValue()))
                .limit(20).forEach(k -> System.out.println(k.getKey() + "\t" + String.format( "%.2f", k.getValue() )));

        System.out.println("\nTotal class significance");
        // Print the counts ordered by value
        totalClassSignificance.entrySet().stream().sorted((k1, k2) -> -k1.getValue().compareTo(k2.getValue()))
                .limit(20).forEach(k -> System.out.println(k.getKey() + "\t" + String.format( "%.2f", k.getValue() )));

        System.out.println("\nTotal property significance");
        // Print the counts ordered by value
        totalPredicateSignificance.entrySet().stream().sorted((k1, k2) -> -k1.getValue().compareTo(k2.getValue()))
                .limit(20).forEach(k -> System.out.println(k.getKey() + "\t" + String.format( "%.2f", k.getValue() )));
    }


}