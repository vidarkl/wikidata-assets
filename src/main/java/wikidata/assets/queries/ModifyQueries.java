package wikidata.assets.queries;

import eu.optiquevqs.graph.*;
import eu.optiquevqs.graph.navigation.*;

import wikidata.assets.*;
import wikidata.assets.queries.transformers.EnsureVqsTriplesTransformer;
import wikidata.assets.queries.transformers.FilterTriplesTransformer;
import wikidata.assets.queries.transformers.AddTypesByMapTransformer;
import wikidata.assets.queries.visitors.CheckVQSQueryVisitor;
import wikidata.assets.queries.visitors.VQSTransformVisitor;

// Java IO
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.*;

// Java Util
import java.util.*;

// Jena ARQ
import org.apache.jena.query.*;
import org.apache.jena.sparql.algebra.*;
import org.apache.jena.sparql.algebra.op.*;
import org.apache.jena.sparql.core.*;
import org.apache.jena.graph.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;



// Take as input a file of queries. Transform them into VQS queries
public class ModifyQueries {

    static int limitLinesPerFile = 0;  // If needed, limit the number of rows to include in each file. Set to 0 to include every line

    static String queryFolderPath = "./files/queries/";  // Where to find queries
    static String navigationGraphKey = "wikidata-2";  // Which navigation graph to filter on.

    static boolean printStats = true;  // Print stats about result
    static boolean runFilter = true;  // Only keep parts of the query that are present in the navigation graph
    static boolean runAddTypes = true;  // Run adding types to variables without type.


    public static void main(String[] args) throws Exception {
        if (!runFilter && runAddTypes) {
            System.out.println("You have to run filter before adding types");
        }
        else {
            List<String> queryFilenames = new ArrayList<String>();
            queryFilenames.add("2017-06-12_2017-07-09.txt");
            queryFilenames.add("2017-07-10_2017-08-06.txt");
            queryFilenames.add("2017-08-07_2017-09-03.txt");
            queryFilenames.add("2017-12-03_2017-12-30.txt");
            queryFilenames.add("2018-01-01_2018-01-28.txt");
            queryFilenames.add("2018-01-29_2018-02-25.txt");
            queryFilenames.add("2018-02-26_2018-03-25.txt");
            for (String fileName : queryFilenames) {
                fixQueriesInFile(fileName);
            }
        }
    }












    // This function reads one single file containing queries, transforms them into VQS and writes to a new file.
    public static void fixQueriesInFile(String fileName) throws Exception  {

        final long startTime = System.currentTimeMillis();

        /////////////////////////////////////////////////////
        // Pre 1: Read file
        /////////////////////////////////////////////////////

        // Pre 1: Read the queries into a list of strings.
        List<String> readQueries = new ArrayList<String>();
        readQueries = Utils.readFileToList(queryFolderPath + "original/" + fileName, null);

        // If a file line limit is set, then just include the first few lines.
        if (limitLinesPerFile != 0) {
            if (limitLinesPerFile < readQueries.size()) {
                readQueries = readQueries.subList(0,limitLinesPerFile);
            }
        }

        /////////////////////////////////////////////////////
        // Pre 2: Parse into Op and add weight
        /////////////////////////////////////////////////////

        // Pre 2: Parse the queries and put into a map. 
        // Get frequency and let that be the starting weight
        // Later if a query is split into multiple other queries, the weight is divided between them
        Map<Op, Integer> opFrequency = new LinkedHashMap<Op, Integer>();
        for (String query : readQueries) {
            Op op = Utils.parseQueryString(query);
            if (op != null) {
                int countForOp = opFrequency.getOrDefault(op, 0);
                opFrequency.put(op, countForOp + 1);
            }
        }

        List<OpWeight> parsedOps = new ArrayList<OpWeight>();
        for (Map.Entry<Op, Integer> opEntry : opFrequency.entrySet()) {
            Op op = opEntry.getKey();
            Integer frequency = opEntry.getValue();
            parsedOps.add(new OpWeight(op, Double.valueOf(frequency)));
        }


        // Catalogue we will use in this method.
        List<OpWeight> catalogue = parsedOps;

        // Printing stats
        if (printStats) {
            System.out.println("\n\n Before fixing: ");
            Utils.printOpStats(catalogue);
        }

        ///////////////////////////////////////////////////////
        // Part 1: Transform
        ///////////////////////////////////////////////////////
        catalogue = transformQueries(catalogue);
        if (printStats) {
            System.out.println("\n\n After transforming: ");
            Utils.printOpStats(catalogue);
        }



        /////////////////////////////////////////////////////
        // Part 2: Filter
        /////////////////////////////////////////////////////
        if (runFilter) {
            catalogue = filterQueries(catalogue);
            if (printStats) {
                System.out.println("\n\n After transforming: ");
                Utils.printOpStats(catalogue);
            }
        }



        /////////////////////////////////////////////////////
        // Part 3: Add Types
        /////////////////////////////////////////////////////
        if (runAddTypes) {
            catalogue = addTypeToQueries(catalogue);
            if (printStats) {
                System.out.println("\n\n After transforming: ");
                Utils.printOpStats(catalogue);
            }
        }


        // Write the results into file
        Utils.writeOpsToFile(catalogue, queryFolderPath + "output/" + fileName);

        final long endTime = System.currentTimeMillis();

        if (printStats) {
            System.out.println("\n\n final fixed queries: ");
            Utils.printOpStats(catalogue);
        }


        System.out.println("-------------------------------------------------------");
        System.out.println("Total execution time (fixing queries): " + (endTime - startTime) + " ms");
        System.out.println("-------------------------------------------------------");
    }


























    // Part 1: The transformation step
    public static List<OpWeight> transformQueries(List<OpWeight> ops) throws Exception  {

        // Step: Use the vqsTransformVisitor to transform each op into possible multiple new ops, where each of them only contains triples and simple filters.
        // Discard everything we cannot transform easily
        List<OpWeight> transformedOps = new ArrayList<OpWeight>();
        for (OpWeight ow : ops) {
            List<Op> newOps = transformSingleQuery(ow.op);  // List of new queries generated from op

            // Put new queries into new list with parent weight divided equally between the new ops
            if (newOps.size() > 0) {
                Double childWeight = ow.weight/(double)newOps.size();  // Split the original weight of 1 equally between each of them
                for (Op childOp : newOps) {
                    transformedOps.add(new OpWeight(childOp, childWeight));
                }
            }
        }

        // Step: In this step we assume every query is a set of triples and filters. Fix all triples. 
        // I.e. make sure they are of the form (var, uri, var), or (var, type, uri)
        List<OpWeight> uriToVarOps = new ArrayList<OpWeight>();
        for (OpWeight ow : transformedOps) {
            Op opWithCleanedTriples = Transformer.transform(new EnsureVqsTriplesTransformer(), ow.op); // Use transformer to fix these triples
            uriToVarOps.add(new OpWeight(opWithCleanedTriples, ow.weight));
        }

        // Step: clean
        // Do a big cleaning of the ops.
        // remove empty queries
        // rename variables
        // remove pointless filters
        // merge duplicates and add weight again
        List<OpWeight> cleanedAfterTransformOps = Utils.cleanOps(uriToVarOps);
        return cleanedAfterTransformOps;
    }



    // Main step in transformation function: Take one op and turn it into many others by using the VQSTransformVisitor.
    public static List<Op> transformSingleQuery(Op mainOp) throws Exception  {
        // Step 1. Remove all limits, distincts, projections, order by and group by on the outer level. This is faster than doing it in the VQSTransformvisitor.
        // This is completely safe since we do not care about anything of this.
        mainOp = removeLimitDistinctEtc(mainOp);

        // Step 2. Run the big VQSTransformVisitor
        // Queue to pick from. The op we pick will be transformed into one or more new ops which are then put into the queue.
        // If we are not able to do any changes to the op, we have to decide to keep or trash
        List<Op> returnList = new ArrayList<Op>();  // The list of queries to return
        Queue<Op> queue = new LinkedList<>();  // The queue we use.
        Set<Op> visitedOps = new HashSet<Op>();  // Set to keep track of which ops we have visited before
        queue.add(mainOp);  // Adding the main op into the queue

        int maxIterations = 1000; // Set number of iterations allowed for each query. It basically tells how many times we should split a query before we give up. 1000 is quite good. More will almost not help.

        int iterationCount = 0;
        while (queue.size() > 0) {
            iterationCount += 1;
            if (iterationCount > maxIterations) return new ArrayList<Op>();

            Op currentOp = queue.remove();
            visitedOps.add(currentOp);

            // For each visit we get a set of new ops one step closer to valid queries
            VQSTransformVisitor visitor = new VQSTransformVisitor(currentOp);
            OpWalker.walk(currentOp, visitor);
            List<Op> newOpsThisIteration = visitor.getOps();

            // When no new iterations for this was made, we are done iterating with it, and we can check what we want to do with it. Either discard it because it is to hard to transform more, or add it because we are done.
            if (newOpsThisIteration.size() == 0) {  
                if (hasVQSShape(currentOp) && !returnList.contains(currentOp) ) {
                    returnList.add(currentOp);
                }
            }
            else {
                queue.addAll(newOpsThisIteration);
            }

        }
        return returnList;
    }

















    // Part 2: The filter step
    public static List<OpWeight> filterQueries(List<OpWeight> ops) throws Exception  {

        // Get the navigation graph
        NavigationGraph navigationGraph = WikidataAssets.getNavigationGraph(navigationGraphKey);

        List<OpWeight> originalOps = ops;

        // Collect allowed predicates
        // Get the list of predicates from navigation graph
        ArrayList<String> allowedPredicates = new ArrayList<String>();
        for (PropertyEdge pe : navigationGraph.edgeSet()) {
            allowedPredicates.add(pe.getLabel());
        }


        // Collect allowed classes
        // Get the list of classes from navigation graph
        ArrayList<String> allowedClasses = new ArrayList<String>();
        for (NavigationGraphNode n: navigationGraph.vertexSet()) {
            allowedClasses.add(n.getLabel());
        }





        // Do the filtering. Only keep valid triples from queries
        List<OpWeight> filteredOps = new ArrayList<OpWeight>();
        for (OpWeight ow : originalOps) {

            FilterTriplesTransformer filterTriplesTransformer = new FilterTriplesTransformer(allowedClasses, allowedPredicates);
            Op opWithCleanedTriples = Transformer.transform(filterTriplesTransformer, ow.op);

            filteredOps.add(new OpWeight(opWithCleanedTriples, ow.weight));
        }

        // Step: Clean. 
        // Removes empty queries, pointless filters, merges etc.
        List<OpWeight> finishedOps = Utils.cleanOps(filteredOps);

        return finishedOps;
    }






    // Part 3: Adding types
    // Split each query into several new queries where types of each concept variable is set.
    // The choice of types depends on the already set types, and which one are allowed by the navigation graph
    public static List<OpWeight> addTypeToQueries(List<OpWeight> opWeights) throws Exception  {

        List<OpWeight> returnOpWeights = new ArrayList<OpWeight>();
        NavigationGraph ng = WikidataAssets.getNavigationGraph(navigationGraphKey);

        for (OpWeight opWeight : opWeights) {
            Op op = opWeight.op;
            Double weight = opWeight.weight;
            if (op instanceof OpFilter){ op = ((OpFilter)op).getSubOp(); }
            BasicPattern pattern = ((OpBGP)op).getPattern();

            // Find all variables in the pattern
            Set <String> variables = new HashSet<String>();
            for (Triple t: pattern) {
                Node s = t.getSubject();
                Node p = t.getPredicate();
                Node o = t.getObject();
                if (s.isVariable()) { variables.add(s.getName()); }
                if (o.isVariable()) { variables.add(o.getName()); }
            }

            // add variables to three different maps
            Map<String, Set<String>> possibleTypes = new HashMap<String, Set<String>>();  // possible according to navigation graph
            Map<String, Set<String>> activeTypes = new HashMap<String, Set<String>>();  // active, i.e. set in the query
            Map<String, Set<String>> finalTypes = new HashMap<String, Set<String>>();  // Final: set of different possible choices based on our algorithms
            for (String variable : variables) {
                possibleTypes.put(variable, new HashSet<String>());
                activeTypes.put(variable, new HashSet<String>());
                finalTypes.put(variable, new HashSet<String>());
            }

            // Fetch the active types. i.e. types added in query
            for (Triple t: pattern) {
                Node s = t.getSubject();
                Node p = t.getPredicate();
                Node o = t.getObject();

                String predicate = p.getURI();
                List<String> typePredicates = Utils.getTypePredicates();

                if (typePredicates.contains(predicate)) {
                    activeTypes.get(s.getName()).add(o.getURI());
                }
            }

            // Add possible types. i.e. types extracted from navigation graph
            for (Triple t: pattern) {
                Node s = t.getSubject();
                Node p = t.getPredicate();
                Node o = t.getObject();

                String predicate = p.getURI();
                List<String> typePredicates = Utils.getTypePredicates();

                Set <PropertyEdge> edges = ng.edgeSet();
                for (PropertyEdge edge : edges) {
                    if (edge.getLabel().equals(predicate)) {

                        // Add to possibletypes
                        possibleTypes.get(s.getName()).add(ng.getEdgeSource(edge).getLabel());
                        if (ng.isObjectProperty(edge)) { possibleTypes.get(o.getName()).add(ng.getEdgeTarget(edge).getLabel()); }
                        if (ng.isDatatypeProperty(edge)) { possibleTypes.get(o.getName()).add(null); }
                    }
                }
            }


            // Decide on the final types:
            // if variable should not be typed, i.e. it is a datattype variable: its set should contain null only
            // if we cannot find any possible types, i.e. the types contradict, 
            for (String variable : variables) {
                Set<String> a = activeTypes.get(variable);
                Set<String> p = possibleTypes.get(variable);
                Set<String> finalSet = new HashSet<String>();

                // if something is a datatype variable, then it should be without type
                if (p.contains(null)) {
                    // check if there are others in there.
                    if (p.size() == 1) {
                        finalSet.add(null);
                    }
                }
                else {
                    if (a.size() == 0) {
                        // if there are no active types, use all possible types
                        finalSet = p;
                    }
                    else {
                        if (p.size() == 0) {
                            // This means that there were no edges into this variable, and hence no restriction. Use all active types.
                            finalSet = a;
                        }
                        else {
                            // If there is one or more active types, check which ones are legal
                            Set<String> intersection = new HashSet<String>(a);
                            intersection.retainAll(p);
                            if (intersection.size() > 0) {
                                finalSet = intersection;
                            }
                        }
                    }
                }
                finalTypes.put(variable, finalSet);
            }


            // Make all possible maps from the finalTypes map.
            Set<Map<String,String>> allVariableToTypeMaps = makeAllMaps(finalTypes);

            Set<Op> newOps = new HashSet<Op>();
            for (Map<String, String> map : allVariableToTypeMaps) {
                // For each map, make a new version of the query
                Set<String> vars = new HashSet<String>(map.keySet());

                // First just remove all maps to null, because they are not supposed to be set anyway.
                for (String variable : vars) {
                    if (map.get(variable) == null) {
                        map.remove(variable);
                    }
                }




                // Some maps from variables to types does not make sense together.
                // One can get a situation where the source and target type does not really exist in ng.
                // In this step we discards such mappings
                Boolean discardMapping = false;
                for (Triple t: pattern) {
                    Node s = t.getSubject();
                    Node p = t.getPredicate();
                    Node o = t.getObject();

                    String predicate = p.getURI();
                    List<String> typePredicates = Utils.getTypePredicates();
                    if (typePredicates.contains(predicate)) continue;

                    // For each predicate we must find a corresponding edge in ng
                    Boolean foundCorrespondingEdge = false;
                    for (PropertyEdge edge : ng.edgeSet()) {
                        if (edge.getLabel().equals(predicate)) {
                            Boolean edgeMatch = true;

                            // The source must match
                            if (!(ng.getEdgeSource(edge).getLabel().equals(map.get(s.getName())))) edgeMatch = false;

                            if (ng.isObjectProperty(edge)) {
                                // the target must match
                                if (!(ng.getEdgeTarget(edge).getLabel().equals(map.get(o.getName())))) edgeMatch = false;
                            }

                            if (edgeMatch) {
                                foundCorrespondingEdge = true;
                                break;
                            }
                        }
                    }
                    if (!foundCorrespondingEdge) {
                        discardMapping = true;
                        break;
                    }
                }
                if (discardMapping) continue;

                // Transform using a transformer
                Op newOp = Transformer.transform(new AddTypesByMapTransformer(map), op); // Use transformer to fix these triples
                newOps.add(newOp);
            }

            // Add all new ops with the correct new weight
            if (newOps.size() != 0) {
                Double newWeight = weight/newOps.size();
                for (Op newOp : newOps) {
                    returnOpWeights.add(new OpWeight(newOp, newWeight));
                }
            }
        }
        return returnOpWeights;
    }



    // Given a map from variables to a set of possible types, make all possible maps from variable to one type.
    // If a variable has an empty set, then the result should also be an empty set.
    // This is a recursive method.
    public static Set<Map<String, String>> makeAllMaps(Map<String, Set<String>> m) {

        // Pop first key/variable
        Set<String> keys = m.keySet();
        String popVariable = null;
        for (String key : keys) { 
            popVariable = key; 
            break; 
        }
        Set<String> popSet = m.get(popVariable);
        Set<Map<String,String>> returnSet = new HashSet<Map<String,String>>();

        // If only one key, then just add a map to each type
        if (keys.size() == 1) {
            for (String type : popSet){
                Map <String, String> s = new HashMap<String, String>();
                s.put(popVariable, type);
                returnSet.add(s);
            }
            return returnSet;
        }
        else {
            // Else, combine all possible types for first variable with result from recursive call to all other variables.
            m.remove(popVariable);
            Set<Map<String, String>> allOtherMaps = makeAllMaps(m);
            for (String type : popSet){
                for (Map<String, String> otherMap : allOtherMaps) {
                    Map<String, String> otherMapCopy = new HashMap<String,String>(otherMap);
                    otherMapCopy.put(popVariable,type);
                    returnSet.add(otherMapCopy);
                }
            }
            return returnSet;
        }
    }















    // Remove any potential slice/limit, then potential distinct, then potential order by
    // This is only done when it is outside the where clause. It does not remove limits inside.
    // It is smart to do this as early as possible, before the queries are duplicated.
    public static Op removeLimitDistinctEtc(Op op) throws Exception  {
        while (op instanceof OpSlice || op instanceof OpDistinct || op instanceof OpProject || op instanceof OpOrder || op instanceof OpGroup) {
            if (op instanceof OpSlice) {
                op = ((OpSlice)op).getSubOp();
            }
            if (op instanceof OpDistinct) {
                op = ((OpDistinct)op).getSubOp();
            }
            if (op instanceof OpProject) {
                op = ((OpProject)op).getSubOp();
            }
            if (op instanceof OpOrder) {
                op = ((OpOrder)op).getSubOp();
            }
            if (op instanceof OpGroup) {
                op = ((OpGroup)op).getSubOp();
            }
        }
        return op;
    }




    // This is used after transformation to check if the query has become valid VQS query
    // A VQS query can only contain one bgp with triples, and a set of simple filters
    public static boolean hasVQSShape(Op op) throws Exception  {
        CheckVQSQueryVisitor visitor = new CheckVQSQueryVisitor();
        OpWalker.walk(op, visitor);
        return visitor.getResult();
    }


}