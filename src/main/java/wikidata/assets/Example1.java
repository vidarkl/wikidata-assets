package wikidata.assets;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

import eu.optiquevqs.graph.*;
import eu.optiquevqs.graph.navigation.*;
import eu.optiquevqs.graph.query.QueryGraph;
import eu.optiquevqs.graph.query.QueryGraphFactory;

public class Example1 {

	public static void main(String[] args) throws Exception {
		NavigationGraph wdng1 = WikidataAssets.getNavigationGraph("wikidata-1");
		System.out.println(wdng1);
		NavigationGraph wdng2 = WikidataAssets.getNavigationGraph("wikidata-2");
		System.out.println(wdng2);


		int fileno = 0; // 0,1,2,3,4,5 or 6

		NavigationGraph wdng = WikidataAssets.getNavigationGraph("wikidata-1");

		String[] files = new String[7];
		String path = "files/queries/output/";
		files[0] = path + "2017-06-12_2017-07-09.txt";
		files[1] = path + "2017-07-10_2017-08-06.txt";
		files[2] = path + "2017-08-07_2017-09-03.txt";
		files[3] = path + "2017-12-03_2017-12-30.txt";
		files[4] = path + "2018-01-01_2018-01-28.txt";
		files[5] = path + "2018-01-29_2018-02-25.txt";
		files[6] = path + "2018-02-26_2018-03-25.txt";

		Scanner fileReader = null;
		try {
			fileReader = new Scanner(new File(files[fileno]));
		} catch (FileNotFoundException e) {
			System.out.println(e);
		}

		while(fileReader.hasNext()) {
			String line = fileReader.nextLine();
			String[] linesplit = line.split("\t", -1);
			double weight = Double.parseDouble(linesplit[0]);
			QueryGraph qg = QueryGraphFactory.create(linesplit[1], wdng);
			qg.setWeight(weight);
			System.out.println(linesplit[1]);
			System.out.println(qg);
			// System.out.println("pseudocycle:  " + qg.detectPseudoCycles());
			// System.out.println("cycle:        " + qg.detectCycles());
			// System.out.println("disconnected: " + !qg.isConnected());
			// System.out.println("---------");
		}


	}
}
