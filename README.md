# Wikidata Assets
Wikidata Assets is a Java library used to prepare and manage Wikidata assets relevant for [OptiqueVQS](https://gitlab.com/ernesto.jimenez.ruiz/OptiqueVQS) (1) or other visual query systems.
The raw data included in the project is from Wikidata, but the software can also be used to prepare and manage data from other sources if needed.  The assets can be used to test or evaluate new features that requires access to a large and realistic setting.
The three supported types of assets are: 
- navigation graphs
- datasets
- query logs


## Navigation Graph
A navigation graph is a graph structure where the vertices are specific classes or datatypes and the edges are properties between them. Users are only allowed to construct queries with patterns that also occurs in the navigation graph. In other words, the navigation graph dictates what the users are allowed to query for.

Navigation graphs are defined in `src/main/java/wikidata/assets/WikidataAssets.java`, and each graph has a corresponding id that is needed in order to fetch it.
The project comes with two wikidata navigation graphs: "wikidata-1" and "wikidata-2".

## Query logs
The queries constructed by OptiqueVQS are tree-shaped and typed, and allow datatype property filters.
Our software can generate a list of this type of queries based on a list of original arbitrary queries. 
The software can also filter the queries to only include parts that are compatible with the navigation graph.

Original queries must be collected into text files with one query on each line and placed in `files/queries/original`.
Queries in this folder can be transformed to make them tree-shaped and typed, and filtered w.r.t the given navigation graph. This whole modification process is described in detail in (2).

The modification process can be configured by changing the parameters in the file `src/main/java/wikidata/assets/queries/ModifyQueries.java`. 
The query files to include in the modification process must also be listed.
When configuration is done, run the following commands to apply the modification (this requires Maven):

```
mvn clean install
mvn exec:java@ModifyQueries
```

When the process is done, the results are stored in the folder `files/queries/output`.

The queries included in the repository are the 3.5 million anonymized human-made queries from 2017 and 2018 provided by the Knowledge-Based Systems group at TU Dresden
([link](https://iccl.inf.tu-dresden.de/web/Wikidata_SPARQL_Logs/en)) under license [CC-0](https://creativecommons.org/publicdomain/zero/1.0/).


## Data
The software filters the data to only include triples that are compatible with the navigation graph.
The original dataset must be placed in `files/data/original` as a Turtle file and the processed output file will be available in `files/data/output`.
To activate the process run the following commands:

```
mvn clean install
mvn exec:java@FilterData
```

No datasets are provided because Wikidata files are very large, but they can be downloaded directly from the Wikidata webpage. The dataset used in the evaluation of (2) is available [here](https://archive.org/download/wikidata-json-20150420).


## References
1. Klungre, V.N., 2020. **Adaptive Query Extension Suggestions for Ontology-
Based Visual Query Systems.** Ph.D. thesis. University of Oslo, 2020, [link](https://www.duo.uio.no/handle/10852/80614)
2. Ahmet Soylu, Evgeny Kharlamov, Dimitry Zheleznyakov, Ernesto Jimenez Ruiz, Martin Giese, Martin G. Skjaeveland, Dag Hovland, Rudolf Schlatte, Sebastian Brandt, Hallstein Lie, Ian Horrocks. **OptiqueVQS: a Visual Query System over Ontologies for Industry**. Semantic Web Journal, 2018. [link](http://semantic-web-journal.net/content/optiquevqs-visual-query-system-over-ontologies-industry-0)
3. Klungre, Ahmet Soylu, Ernesto Jimenez-Ruiz, Evgeny Kharlamov, Martin Giese. **Query extension suggestions for visual query systems through ontology projection and indexing**. New Generation Computing, 2019 [link](https://link.springer.com/article/10.1007/s00354-019-00071-1)
4. S. Malyshev, M. Krötzsch, L. González, J. Gonsior and A. Bielefeldt. **Getting the Most out of Wikidata: Semantic Technology Us- age in Wikipedia’s Knowledge Graph**. Proceedings of the 17th International Semantic Web Conference (ISWC’18), D. Vrandecic, K. Bontcheva, M.C. Suárez-Figueroa, V. Presutti, I. Celino, M. Sabou, L.-A. Kaffee and E. Simperl, eds, LNCS, Vol. 11137, Springer, 2018, pp. 376–394.

